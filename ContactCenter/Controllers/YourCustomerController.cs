﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter;
using ContactCenter.Common;
using ContactCenter.Models;

namespace ContactCenter.Controllers
{
    public class YourCustomerController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();

        // GET: YourCustomer
        public ActionResult Index()
        {
            int userId = (int)Session["UserID"];
            var cUSTOMERs = db.CUSTOMERs.Include(c => c.USER).Where(w => w.CUSTOMERID != 0 && w.CREATEDBY == userId);
            return View(cUSTOMERs.ToList());
        }

        // GET: YourCustomer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            else if(cUSTOMER.CREATEDBY != (int)Session["UserID"])
            {
                return RedirectToAction("NotPermitted", "Error");
            }

            return View(cUSTOMER);
        }

        // GET: YourCustomer/Create
        public ActionResult Create()
        {
            ViewBag.CITY = db.CITies.OrderBy(o => o.CITY1).ToList();

            return View();
        }

        // POST: YourCustomer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CUSTOMER cUSTOMER)
        {
            //ViewBag.CITY = Helpers.GetCityList();
            ViewBag.CITY = db.CITies.OrderBy(o => o.CITY1).ToList();

            ModelState.Remove("CUSTOMERCODE");
            ModelState.Remove("PHONENUMBER");
            ModelState.Remove("EMAIL");
            ModelState.Remove("ADDRESS");
            ModelState.Remove("PLACEOFPRACTICE1");
            ModelState.Remove("STATUS");

            CUSTOMER chkCustomer = db.CUSTOMERs.Where(w => w.CUSTOMERCODE == cUSTOMER.CUSTOMERCODE).FirstOrDefault();

            if (chkCustomer != null)
            {
                ModelState.AddModelError("CUSTOMERCODE", "Customer Code is already exists");
            }
            //if (cUSTOMER.CUSTOMERCODE == null)
            //{
            //    ModelState.AddModelError("CUSTOMERCODE", "Customer Code is required");
            //}
            if (cUSTOMER.FULLNAME == null)
            {
                ModelState.AddModelError("FULLNAME", "Fullname is required");
            }
            if (cUSTOMER.GENDER == null)
            {
                ModelState.AddModelError("GENDER", "Gender is required");
            }
            if (cUSTOMER.CITY == null)
            {
                ModelState.AddModelError("CITY", "City is required");
            }

            if (ModelState.IsValid)
            {
                cUSTOMER.CREATEDDATE = DateTime.Now;
                cUSTOMER.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                cUSTOMER.STATUS = 2;

                try
                {
                    if (cUSTOMER.CUSTOMERCODE == null)
                    {
                        cUSTOMER.CUSTOMERCODE = GenerateCustomerCode();
                    }

                    db.CUSTOMERs.Add(cUSTOMER);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New Customer Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(cUSTOMER);
                }

                return RedirectToAction("Index");
            }

            return View(cUSTOMER);
        }

        private string GenerateCustomerCode()
        {
            string newId = "";
            string lastId = db.CUSTOMERs.Where(w => w.CUSTOMERCODE.StartsWith("CS")).Max(m => m.CUSTOMERCODE);
            int number = Convert.ToInt16(lastId.Substring(2, lastId.Length - 2));

            newId = "CS" + (++number).ToString().PadLeft(4, '0');

            return newId;
        }

        // GET: YourCustomer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            else if (cUSTOMER.CREATEDBY != (int)Session["UserID"])
            {
                return RedirectToAction("NotPermitted", "Error");
            }

            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            return View(cUSTOMER);
        }

        // POST: YourCustomer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CUSTOMER cUSTOMER)
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            ModelState.Remove("CUSTOMERCODE");
            ModelState.Remove("PHONENUMBER");
            ModelState.Remove("EMAIL");
            ModelState.Remove("ADDRESS");
            ModelState.Remove("PLACEOFPRACTICE1");
            ModelState.Remove("STATUS");

            CUSTOMER chkCustomer = db.CUSTOMERs.Where(w => w.CUSTOMERCODE == cUSTOMER.CUSTOMERCODE && w.CUSTOMERID != cUSTOMER.CUSTOMERID).FirstOrDefault();

            if (chkCustomer != null)
            {
                ModelState.AddModelError("CUSTOMERCODE", "Customer Code is already exists");
            }
            if (cUSTOMER.FULLNAME == null)
            {
                ModelState.AddModelError("FULLNAME", "Fullname is required");
            }
            if (cUSTOMER.GENDER == null)
            {
                ModelState.AddModelError("GENDER", "Gender is required");
            }
            if (cUSTOMER.CITY == null)
            {
                ModelState.AddModelError("CITY", "City is required");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var cust = db.CUSTOMERs.AsNoTracking().Where(w => w.CUSTOMERID == cUSTOMER.CUSTOMERID).SingleOrDefault();
                    
                    if(cust.STATUS != 2)
                    {
                        ViewBag.Error = "You cannot Edit this Customer, because it's not in Approval Status";
                        //throw;
                        return View(cUSTOMER);
                    }
                    else
                    {
                        cUSTOMER.STATUS = 2;
                    }

                    db.Entry(cUSTOMER).State = EntityState.Modified;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update Customer Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(cUSTOMER);
                }

                return RedirectToAction("Index");
            }

            return View(cUSTOMER);
        }

        // GET: YourCustomer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // POST: YourCustomer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            db.CUSTOMERs.Remove(cUSTOMER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
