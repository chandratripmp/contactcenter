﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter;
using ContactCenter.Common;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ContactCenter.Controllers
{
    public class ProductController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();

        // GET: Product
        public ActionResult Index()
        {
            var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
            return View(pRODUCTs.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT pRODUCT = db.PRODUCTs.Find(id);
            if (pRODUCT == null)
            {
                return HttpNotFound();
            }
            return View(pRODUCT);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PRODUCT pRODUCT)
        {
            PRODUCT chkProduct = db.PRODUCTs.Where(w => w.PRODUCTCODE == pRODUCT.PRODUCTCODE).FirstOrDefault();

            //if (chkProduct != null)
            //{
            //    ModelState.AddModelError("PRODUCTCODE", "Product Code is already exists");
            //}

            if (ModelState.IsValid)
            {
                pRODUCT.CREATEDDATE = DateTime.Now;
                pRODUCT.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());

                try
                {
                    db.PRODUCTs.Add(pRODUCT);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New Product Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(pRODUCT);
                }
                
                return RedirectToAction("Index");
            }

            return View(pRODUCT);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT pRODUCT = db.PRODUCTs.Find(id);
            if (pRODUCT == null)
            {
                return HttpNotFound();
            }
            return View(pRODUCT);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PRODUCT pRODUCT)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(pRODUCT).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update Product Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(pRODUCT);
                }
                
                return RedirectToAction("Index");
            }
            return View(pRODUCT);
        }

        // GET: Product/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    PRODUCT pRODUCT = db.PRODUCTs.Find(id);
        //    if (pRODUCT == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pRODUCT);
        //}

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRODUCT pRODUCT = db.PRODUCTs.Find(id);
            db.PRODUCTs.Remove(pRODUCT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Import()
        {
            var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
            return View("Index", pRODUCTs.ToList());
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            //LogSystem logging = new LogSystem();
            try
            {
                if (excelfile == null || excelfile.ContentLength == 0)
                {
                    ViewBag.Error = "Please select an Excel File";

                    var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
                    return View("Index", pRODUCTs.ToList());
                }
                else
                {
                    if (excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx") || excelfile.FileName.EndsWith("xlsb"))
                    {
                        string consString = ConfigurationManager.ConnectionStrings["SQLConnect1"].ConnectionString;
                        //SQLConnect connSQL = new SQLConnect(consString);
                        
                        string fileName = "";
                        if (excelfile.FileName.Contains('/'))
                        {
                            string[] aa = excelfile.FileName.Split('/');
                            fileName = aa[aa.Length - 1];
                        }
                        else
                        {
                            if (excelfile.FileName.Contains('\\'))
                            {
                                string[] aa = excelfile.FileName.Split('\\');
                                fileName = aa[aa.Length - 1];
                            }
                            else
                            {
                                fileName = excelfile.FileName;
                            }
                        }
                        string extension = System.IO.Path.GetExtension(excelfile.FileName);
                        string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFolder"), fileName);

                        if (System.IO.File.Exists(path1))
                            System.IO.File.Delete(path1);

                        excelfile.SaveAs(path1);

                        string excelConnectionString = string.Empty;

                        excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                        //OleDbConnection conn = new OleDbConnection(excelConnectionString);

                        using (OleDbConnection conn = new OleDbConnection(excelConnectionString))
                        {
                            try
                            {
                                if (conn.State == ConnectionState.Closed)
                                    conn.Open();

                                DataTable dtExcelSchema;
                                dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = "DataRaw";

                                string query = "SELECT * FROM [" + sheetName + "$]";
                                //string query = "SELECT * FROM [Sheet1$]";
                                OleDbCommand cmd = new OleDbCommand(query, conn);
                                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                                DataSet ds = new DataSet();

                                da.Fill(ds);
                                DateTime today = DateTime.Now;

                                // validate upload
                                if (!Validate(ds.Tables[0]))
                                {
                                    ViewBag.Error = "See error message below";

                                    var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
                                    return View("Index", pRODUCTs.ToList());
                                }

                                int val = 0;

                                using (var dbTran = db.Database.BeginTransaction())
                                {
                                    var pRODUCTs = db.PRODUCTs.Include(p => p.USER);

                                    try
                                    {
                                        // Update all product to Inactive
                                        db.PRODUCTs
                                            .Where(w => w.STATUS == (int)Status.Active)
                                            .ToList()
                                            .ForEach(a =>
                                                {
                                                    a.STATUS = (int) Status.Inactive;
                                                    a.CREATEDDATE = a.CREATEDDATE;
                                                    a.CREATEDBY = a.CREATEDBY;
                                                }
                                            );
                                        db.SaveChanges();
                                        
                                        DataRow[] datas = ds.Tables[0].AsEnumerable().Where(s => s.Field<string>("WAREHOUSE") == "Saleable").ToArray();

                                        foreach (DataRow dr in datas)
                                        {
                                            string product_code = dr["ZP_ITEM_CODE"].ToString().Trim().TrimStart().TrimEnd();
                                            string product_name = dr["ITEM_NAME"].ToString().Trim().TrimStart().TrimEnd();
                                            string batch_no     = dr["BATCH"].ToString().Trim().TrimStart().TrimEnd();
                                            string expired_date = dr["EXPDATE"].ToString().Trim().TrimStart().TrimEnd();
                                            string price        = dr["LIST_PRICE_A"].ToString().Trim().TrimStart().TrimEnd();
                                            string plant        = dr["BRANCH"].ToString().Trim().TrimStart().TrimEnd();
                                            string qty          = dr["AVAILABLE_QTY"].ToString().Trim().TrimStart().TrimEnd();
                                            
                                            string s_expired_date = expired_date.Substring(0, 4) + "/" + expired_date.Substring(4, 2) + "/" + expired_date.Substring(6, 2);

                                            PRODUCT p     = new PRODUCT();
                                            p.PRODUCTCODE = product_code;
                                            p.PRODUCTNAME = product_name;
                                            p.BATCHNO     = batch_no;
                                            p.EXPIREDDATE = Convert.ToDateTime(s_expired_date);
                                            p.PRICE       = Convert.ToDouble(price);
                                            p.PLANT       = plant;
                                            p.STOCK       = Convert.ToDecimal(qty.Replace(',','.'));
                                            p.STATUS      = (int)Status.Active;
                                            p.CREATEDBY   = Convert.ToInt16(Session["UserID"].ToString());
                                            p.CREATEDDATE = DateTime.Now;
                                            db.PRODUCTs.Add(p);
                                        }
                                        db.SaveChanges();

                                        dbTran.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        dbTran.Rollback();
                                        LogSystem.ErrorLog(ex, null);
                                        ViewBag.Error = "Data failed to insert";

                                        return View("Index", pRODUCTs.ToList());
                                    }

                                    ViewBag.Success = "Import Product Success";
                                    return View("Index", pRODUCTs.ToList());
                                }
                            }
                            catch (Exception)
                            {
                                conn.Close();
                                throw;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Error = "File type is incorrect <br>";

                        var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
                        return View("Index", pRODUCTs.ToList());
                    }

                }
            }
            catch (Exception ex)
            {
                //logging.ErrorLog(ex, null);
                LogSystem.ErrorLog(ex, null);
                ViewBag.Error = "Failed, your template file might be wrong. Please contact your Administrator! <br>";

                var pRODUCTs = db.PRODUCTs.Include(p => p.USER);
                return View("Index", pRODUCTs.ToList());
            }

            //finally
            //{
            //    return View("Success");
            //}
        }

        [HttpPost]
        public ActionResult LoadProduct()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //Find Search Keyword
            var searchKey = Request.Form.GetValues("search[value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
            
            //List<PRODUCT> v = db.PRODUCTs.ToList();
            List<ProductList> v = db.PRODUCTs.AsEnumerable().Select(s => new ProductList
            {
                PRODUCTID = s.PRODUCTID.ToString(),
                PRODUCTCODE = s.PRODUCTCODE,
                PRODUCTNAME = s.PRODUCTNAME,
                BATCHNO = s.BATCHNO,
                EXPIREDDATE = s.EXPIREDDATE.Value.ToString("dd MMM yyyy"),
                STATUS = s.STATUS.ToString(),
                CREATEDDATE = s.CREATEDDATE.ToString("dd/MM/yyyy H:mm:ss"),
                CREATEDBY = s.USER.USERNAME,
                O_CREATEDDATE = s.CREATEDDATE.ToString("yyyyMMddHmmss")
            }).ToList();

            //FILTER
            if (!string.IsNullOrEmpty(searchKey))
            {
                v = v.Where(
                        w => 
                        w.PRODUCTCODE.Contains(searchKey)
                        || w.PRODUCTNAME.Contains(searchKey)
                        || w.BATCHNO.Contains(searchKey)
                    ).ToList();
            }

            //SORT
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                sortColumn = sortColumn == "CREATEDDATE" ? "O_CREATEDDATE" : sortColumn;
                var pi = typeof(ProductList).GetProperty(sortColumn);
                
                if(sortColumnDir == "asc")
                {
                    v = v.OrderBy(x => pi.GetValue(x, null)).ToList();
                }
                else
                {
                    v = v.OrderByDescending(x => pi.GetValue(x, null)).ToList();
                }
            }

            recordsTotal = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        // validate upload
        private bool Validate(DataTable dt)
        {
            int row = 1;
            int error = 0;
            string[,] msgArr = new string[dt.Rows.Count, 2];
            
            if(!dt.AsEnumerable().Any(a => a.Field<string>("WAREHOUSE") == "Saleable"))
            {
                msgArr[0, 0] = "[whole file]";
                msgArr[0, 1] = "File not contains <b>Saleable</b> [WAREHOUSE] data";

                ViewBag.ErrorUpload = msgArr;
                return false;
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    int rowError = 0;
                    string msg = "";

                    if(dr["WAREHOUSE"].ToString() == "Saleable")
                    {
                        // modify here
                        string product_code = dr["ZP_ITEM_CODE"].ToString();
                        string product_name = dr["ITEM_NAME"].ToString();
                        string expired_date = dr["EXPDATE"].ToString();
                        string batch_no = dr["BATCH"].ToString();
                        string price = dr["LIST_PRICE_A"].ToString();
                        string plant = dr["BRANCH"].ToString();
                        string qty = dr["AVAILABLE_QTY"].ToString();
                        string warehouse = dr["WAREHOUSE"].ToString();

                        if (product_code == "")
                        {
                            msg += "Product Code [ZP_ITEM_CODE] is required<br>";

                            rowError++;
                        }

                        if (product_name == "")
                        {
                            msg += "Product Name [ITEM_NAME] is required<br>";

                            rowError++;
                        }

                        if (expired_date == "")
                        {
                            msg += "Expired Date [EXPDATE] is required<br>";

                            rowError++;
                        }
                        else if (!Regex.IsMatch(expired_date, "^([12]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01]))$"))
                        {
                            msg += "Expired Date [EXPDATE] format is incorrect. Please input with YYYYMMDD format (ex: 19991231)<br>";

                            rowError++;
                        }


                        if (batch_no == "")
                        {
                            msg += "Batch No [BATCH] is required<br>";

                            rowError++;
                        }

                        if (price == "")
                        {
                            msg += "Price [LIST_PRICE_A] is required<br>";

                            rowError++;
                        }
                        else if (!Regex.IsMatch(price, "^[0-9]*$"))
                        {
                            msg += "Price [LIST_PRICE_A] accepts only number<br>";

                            rowError++;
                        }

                        if (plant == "")
                        {
                            msg += "Plant [BRANCH] is required<br>";

                            rowError++;
                        }

                        if (qty == "")
                        {
                            msg += "Stock [AVAILABLE_QTY] is required<br>";

                            rowError++;
                        }
                        else if (!Regex.IsMatch(qty, "^([0-9]*)([,.][0-9]*)?$"))
                        {
                            msg += "Stock [AVAILABLE_QTY] accepts only number<br>";

                            rowError++;
                        }
                        // modify until here

                        if (rowError > 0)
                        {
                            msgArr[error, 0] = row.ToString();
                            msgArr[error, 1] = msg.EndsWith("<br>") ? msg.Substring(0, msg.Length - 4) : msg;
                            error++;
                        }
                    }

                    row++;
                }
            }
            
            if (error > 0)
            {
                ViewBag.ErrorUpload = msgArr;
                return false;
            }
            else
                return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class ProductList
    {
        public string PRODUCTID { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string BATCHNO { get; set; }
        public string EXPIREDDATE { get; set; }
        public string STATUS { get; set; }
        public string CREATEDDATE { get; set; }
        public string O_CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
    }
}
