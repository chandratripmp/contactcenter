﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter.Common;
using System.IO;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using ContactCenter.Models;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Linq.Dynamic;
namespace ContactCenter.Controllers
{
    public class ChannelController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();

        // GET: Pharmacy
        public ActionResult Index()
        {
            var pHARMACies = db.PHARMACies.Include(p => p.USER);
            return View(pHARMACies.ToList());
        }

        public ActionResult Download()
        {
            string filename = "Upload Channel Template.xlsx";
            string file = Server.MapPath("~/Content/Templates/" + filename);
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(file, contentType, Path.GetFileName(file));
        }

        [HttpGet]
        public ActionResult Import()
        {
            var pHARMACies = db.PHARMACies.Include(p => p.USER);
            return View("Index", pHARMACies.ToList());
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            //LogSystem logging = new LogSystem();
            try
            {
                if (excelfile == null || excelfile.ContentLength == 0)
                {
                    ViewBag.Error = "Please select an Excel File";

                    var pHARMACies = db.PHARMACies.Include(p => p.USER);
                    return View("Index", pHARMACies.ToList());
                }
                else
                {
                    if (excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx") || excelfile.FileName.EndsWith("xlsb"))
                    {
                        //string consString = ConfigurationManager.ConnectionStrings["SQLConnect1"].ConnectionString;
                        //SQLConnect connSQL = new SQLConnect(consString);

                        string fileName = "";
                        if (excelfile.FileName.Contains('/'))
                        {
                            string[] aa = excelfile.FileName.Split('/');
                            fileName = aa[aa.Length - 1];
                        }
                        else
                        {
                            if (excelfile.FileName.Contains('\\'))
                            {
                                string[] aa = excelfile.FileName.Split('\\');
                                fileName = aa[aa.Length - 1];
                            }
                            else
                            {
                                fileName = excelfile.FileName;
                            }
                        }
                        string extension = System.IO.Path.GetExtension(excelfile.FileName);
                        string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFolder"), fileName);

                        if (System.IO.File.Exists(path1))
                            System.IO.File.Delete(path1);

                        excelfile.SaveAs(path1);

                        string excelConnectionString = string.Empty;

                        excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                        //excelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source = '" + path1 + "';Extended Properties=\"Excel 8.0;HDR=YES;\"";
                   

                        //OleDbConnection conn = new OleDbConnection(excelConnectionString);

                        using (OleDbConnection conn = new OleDbConnection(excelConnectionString))
                        {
                            try
                            {
                                if (conn.State == ConnectionState.Closed)
                                    conn.Open();

                                DataTable dtExcelSchema;
                                dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                                string sheetName = "Sheet1";
                                string query = "SELECT * FROM [" + sheetName + "$]";

                                OleDbCommand cmd = new OleDbCommand(query, conn);
                                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                                DataSet ds = new DataSet();

                                da.Fill(ds);
                                DateTime today = DateTime.Now;

                                // validate upload
                                if (!Validate(ds.Tables[0]))
                                {
                                    ViewBag.Error = "See error message below";

                                    var pHARMACies = db.PHARMACies.Include(p => p.USER);
                                    return View("Index", pHARMACies.ToList());
                                }

                                int val = 0;

                                using (var dbTran = db.Database.BeginTransaction())
                                {
                                    var pHARMACies = db.PHARMACies.Include(p => p.USER);

                                    try
                                    {
                                        foreach (DataRow dr in ds.Tables[0].Rows)
                                        {
                                            string channel_code   = dr["CHANNEL_CODE"].ToString().Trim().TrimStart().TrimEnd();
                                            string channel_name   = dr["CHANNEL_NAME"].ToString().Trim().TrimStart().TrimEnd();
                                            string phone_number   = dr["PHONE_NUMBER"].ToString().Trim().TrimStart().TrimEnd();
                                            string phone_number2  = dr["PHONE_NUMBER2"].ToString().Trim().TrimStart().TrimEnd();
                                            string email          = dr["EMAIL"].ToString().Trim().TrimStart().TrimEnd();
                                            string city           = dr["CITY"].ToString().Trim().TrimStart().TrimEnd();
                                            string address        = dr["ADDRESS"].ToString().Trim().TrimStart().TrimEnd();
                                            string type           = dr["TYPE"].ToString().Trim().TrimStart().TrimEnd();
                                            string plant          = dr["PLANT"].ToString().Trim().TrimStart().TrimEnd();
                                            // TODO: panel not used yet
                                            string panel          = dr["PANEL"].ToString().Trim().TrimStart().TrimEnd();

                                            int i_city = db.CITies.Where(w => w.CITY1 == city).Select(s => s.ID).SingleOrDefault();

                                            PHARMACY c = new PHARMACY();
                                            c.PHARMACYCODE  = channel_code;
                                            c.PHARMACYNAME  = channel_name;
                                            c.PHONENUMBER   = phone_number;
                                            c.PHONENUMBER2 = phone_number2;
                                            c.EMAIL         = email;
                                            c.CITY          = i_city;
                                            c.ADDRESS       = address;
                                            c.TYPE          = char.ToUpper(type[0]) + type.ToLower().Substring(1);
                                            c.PHARMACYPLANT = plant;
                                            // TODO: panel not used yet
                                            //c.PANEL = panel == "Y" ? 1 : 0;

                                            c.STATUS = (int)Status.Active;
                                            c.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                                            c.CREATEDDATE = DateTime.Now;
                                            db.PHARMACies.Add(c);
                                        }
                                        db.SaveChanges();

                                        dbTran.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        dbTran.Rollback();
                                        LogSystem.ErrorLog(ex, null);
                                        ViewBag.Error = "Data failed to insert";

                                        return View("Index", pHARMACies.ToList());
                                    }

                                    ViewBag.Success = "Import Channel Success";
                                    return View("Index", pHARMACies.ToList());
                                }
                            }
                            catch (Exception exx)
                            {
                                conn.Close();
                                throw;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Error = "File type is incorrect";
                        
                        var pHARMACies = db.PHARMACies.Include(p => p.USER);
                        return View("Index", pHARMACies.ToList());
                    }

                }
            }
            catch (Exception ex)
            {
                //logging.ErrorLog(ex, null);
                LogSystem.ErrorLog(ex, null);
                ViewBag.Error = "Failed, your template file might be wrong. Please contact your Administrator!\n"+ ex;

                var pHARMACies = db.PHARMACies.Include(p => p.USER);
                return View("Index", pHARMACies.ToList());
            }

            //finally
            //{
            //    return View("Success");
            //}
        }

        // GET: Pharmacy/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PHARMACY pHARMACY = db.PHARMACies.Find(id);
            if (pHARMACY == null)
            {
                return HttpNotFound();
            }
            return View(pHARMACY);
        }

        // GET: Pharmacy/Create
        public ActionResult Create()
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            return View();
        }

        // POST: Pharmacy/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PHARMACY pHARMACY)
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            PHARMACY chkCustomer = db.PHARMACies.Where(w => w.PHARMACYCODE == pHARMACY.PHARMACYCODE).FirstOrDefault();

            if (chkCustomer != null)
            {
                ModelState.AddModelError("PHARMACYCODE", "Pharmacy Code is already exists");
            }

            if (ModelState.IsValid)
            {
                pHARMACY.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                pHARMACY.CREATEDDATE = DateTime.Now;
                try
                {
                    db.PHARMACies.Add(pHARMACY);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New Pharmacy Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(pHARMACY);
                }

                return RedirectToAction("Index");
            }

            return View(pHARMACY);
        }

        // GET: Pharmacy/Edit/5
        public ActionResult Edit(int? id)
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PHARMACY pHARMACY = db.PHARMACies.Find(id);
            if (pHARMACY == null)
            {
                return HttpNotFound();
            }

            return View(pHARMACY);
        }

        // POST: Pharmacy/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PHARMACY pHARMACY)
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(pHARMACY).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update Pharmacy Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(pHARMACY);
                }

                return RedirectToAction("Index");
            }

            return View(pHARMACY);
        }

        // GET: Pharmacy/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    PHARMACY pHARMACY = db.PHARMACies.Find(id);
        //    if (pHARMACY == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(pHARMACY);
        //}

        // POST: Pharmacy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PHARMACY pHARMACY = db.PHARMACies.Find(id);
            db.PHARMACies.Remove(pHARMACY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // validate upload
        private bool Validate(DataTable dt)
        {
            int row = 1;
            int error = 0;
            string[,] msgArr = new string[dt.Rows.Count, 2];

            Dictionary<string, string> compDet = new Dictionary<string, string>();

            foreach (DataRow dr in dt.Rows)
            {
                int rowError = 0;
                string msg = "";

                // modify here
                string channel_code = dr["CHANNEL_CODE"].ToString().Trim().TrimStart().TrimEnd();
                string channel_name = dr["CHANNEL_NAME"].ToString().Trim().TrimStart().TrimEnd();
                string phone_number = dr["PHONE_NUMBER"].ToString().Trim().TrimStart().TrimEnd();
                string email        = dr["EMAIL"].ToString().Trim().TrimStart().TrimEnd();
                string city         = dr["CITY"].ToString().Trim().TrimStart().TrimEnd();
                string address      = dr["ADDRESS"].ToString().Trim().TrimStart().TrimEnd();
                string type         = dr["TYPE"].ToString().Trim().TrimStart().TrimEnd();
                string plant        = dr["PLANT"].ToString().Trim().TrimStart().TrimEnd();
                // TODO: panel not used yet
                string panel        = dr["PANEL"].ToString().Trim().TrimStart().TrimEnd();

                if (channel_code == "")
                {
                    msg += "[CHANNEL_CODE] is required<br>";

                    rowError++;
                }
                else if (db.PHARMACies.Any(a => a.PHARMACYCODE == channel_code))
                {
                    msg += "[CHANNEL_CODE] <b>" + channel_code + "</b> already exist<br>";

                    rowError++;
                }

                if (channel_name == "")
                {
                    msg += "[CHANNEL_NAME] is required<br>";

                    rowError++;
                }
                
                if (phone_number == "")
                {
                    msg += "[PHONE_NUMBER] is required<br>";

                    rowError++;
                }
                else if (!Regex.IsMatch(phone_number, "^[0-9]*$"))
                {
                    msg += "[PHONE_NUMBER] accepts only number<br>";

                    rowError++;
                }

                if (email == "")
                {
                    msg += "[EMAIL] is required<br>";

                    rowError++;
                }
                else if (!Regex.IsMatch(email, "^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$"))
                {
                    msg += "[EMAIL] must be in correct format (ex: test@mail.com)<br>";

                    rowError++;
                }

                if (city == "")
                {
                    msg += "[CITY] is required<br>";

                    rowError++;
                }
                // TODO: change Get City List
                else if (!db.CITies.Any(a => a.CITY1 == city))
                {
                    msg += "[CITY] <b>" + city + "</b> is not found in database<br>";

                    rowError++;
                }

                if (address == "")
                {
                    msg += "[ADDRESS] is required<br>";

                    rowError++;
                }

                if (type == "")
                {
                    msg += "[TYPE] is required<br>";

                    rowError++;
                }
                else if(type.ToUpper() != "CLINIC" && type.ToUpper() != "PHARMACY" && type.ToUpper() != "HOSPITAL")
                {
                    msg += "[TYPE] accepts Clinic or Pharmacy or Hospital only<br>";

                    rowError++;
                }

                if (panel == "")
                {
                    msg += "[PANEL] is required<br>";

                    rowError++;
                }
                else if (panel.ToUpper() != "Y" && panel.ToUpper() != "N")
                {
                    msg += "[PANEL] must be Y or N only<br>";

                    rowError++;
                }
                // modify until here

                if (rowError > 0)
                {
                    msgArr[error, 0] = row.ToString();
                    msgArr[error, 1] = msg.EndsWith("<br>") ? msg.Substring(0, msg.Length - 4) : msg;
                    error++;
                }

                row++;
            }

            if (error > 0)
            {
                ViewBag.ErrorUpload = msgArr;
                return false;
            }
            else
                return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Export()
        {
            List<ChannelExport> v = (from a in db.PHARMACies
                                     join cit in db.CITies on a.CITY equals cit.ID
                                     select new { a,cit }).Select(s => new ChannelExport
                                      {
                                          CHANNEL_CODE = s.a.PHARMACYCODE.ToString(),
                                          CHANNEL_NAME = s.a.PHARMACYNAME.ToString(),
                                          PHONENUMBER = s.a.PHONENUMBER.ToString(),
                                          PHONENUMBER2 = s.a.PHONENUMBER2.ToString(),
                                          EMAIL = s.a.EMAIL.ToString(),
                                          CITY = s.cit.CITY1.ToString(),
                                          ADDRESS = s.a.ADDRESS.ToString(),
                                          TYPE = s.a.TYPE.ToString(),
                                          PLANT = s.a.PHARMACYPLANT.ToString(),
                                        
                                      }).ToList();

            string filename = "[Contact Center] Channel Report_" + DateTime.Now.ToString("yyyyMMdd");

            var gv = new GridView();
            gv.DataSource = v;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }
    }
}
