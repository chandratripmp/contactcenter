﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter;
using ContactCenter.Common;
using ContactCenter.Models;
using System.Threading.Tasks;
using ContractCenter.Common;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Linq.Dynamic;
using System.Configuration;

namespace ContactCenter.Controllers
{
    public class OrderController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();

        // GET: Order
        public ActionResult Index()
        {
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();
            ViewBag.List = null;
            //var oRDERDETAILs = db.ORDERDETAILs.Include(i => i.ORDER.CUSTOMER).Include(i => i.ORDER.PHARMACY).Include(i => i.PRODUCT);

            var OrderList = (from o in db.ORDERs
                            join od in db.ORDERDETAILs
                            on o.ORDERID equals od.ORDERID into JoinedEmpDept
                            from od in JoinedEmpDept.DefaultIfEmpty()
                            select new OrderIndexViewModel
                            {
                                ORDER = o,
                                ORDERDETAIL = od
                            }).ToList();

            if((UserRole)Session["UserRole"] == UserRole.ContactCenter)
            {
                int iUserID = Convert.ToInt32(Session["UserID"].ToString());
                OrderList = OrderList.Where(w => w.ORDER.CREATEDBY == iUserID).ToList();
            }

            return View(OrderList.ToList());
        }

        public async Task<ActionResult> TestEmail(int? id)
        {
            ORDER o = db.ORDERs.Find(id);

            await SendNotifyEmail((int)OrderStatus.Complete, o);

            return RedirectToAction("Index");
        }

        public ActionResult CompleteList()
        {
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();
            ViewBag.List = (int)OrderStatus.Confirmed;
            //var oRDERDETAILs = db.ORDERDETAILs.Include(i => i.ORDER.CUSTOMER).Include(i => i.ORDER.PHARMACY).Include(i => i.PRODUCT).Where(w => w.ORDER.STATUS == (int)OrderStatus.Submitted);

            var OrderList = (from o in db.ORDERs
                            join od in db.ORDERDETAILs
                            on o.ORDERID equals od.ORDERID into JoinedEmpDept
                            from od in JoinedEmpDept.DefaultIfEmpty()
                            where o.STATUS == (int)OrderStatus.Confirmed
                            select new OrderIndexViewModel
                            {
                                ORDER = o,
                                ORDERDETAIL = od
                            }).ToList();
            
            if ((UserRole)Session["UserRole"] == UserRole.ContactCenter)
            {
                int iUserID = Convert.ToInt32(Session["UserID"].ToString());
                OrderList = OrderList.Where(w => w.ORDER.CREATEDBY == iUserID).ToList();
            }

            return View(OrderList.ToList());
        }

        public async Task<ActionResult> Complete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ORDER oRDER = new ORDER();

            oRDER = db.ORDERs.Find(id);
            oRDER.STATUS = (int)OrderStatus.Complete;

            try
            {
                db.Entry(oRDER).State = EntityState.Modified;
                db.SaveChanges();

                await SendNotifyEmail((int)OrderStatus.Complete, oRDER);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return RedirectToAction("CompleteList");
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ORDER oRDER = new ORDER();

            oRDER = db.ORDERs.Find(id);
            oRDER.STATUS = (int)OrderStatus.Confirmed;

            try
            {
                db.Entry(oRDER).State = EntityState.Modified;
                db.SaveChanges();

                await SendNotifyEmail((int)OrderStatus.Confirmed, oRDER);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Reject(int? ORDERID, string REASON, string ddlReason)
        {
            if (ORDERID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string s_reason = ddlReason == "" ? REASON : ddlReason;
            ORDER oRDER = new ORDER();

            oRDER = db.ORDERs.Find(ORDERID);
            oRDER.REJECTREASON = s_reason;
            oRDER.STATUS = (int)OrderStatus.Rejected;

            try
            {
                db.Entry(oRDER).State = EntityState.Modified;
                db.SaveChanges();

                await SendNotifyEmail((int)OrderStatus.Rejected, oRDER);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderViewModel OVM = new OrderViewModel();

            OVM.ORDER = db.ORDERs.Find(id);
            OVM.ORDERDETAILList = OVM.ORDER.ORDERDETAILs.ToList();

            if (OVM.ORDER == null)
            {
                return HttpNotFound();
            }

            return View(OVM);
        }

        [HttpGet]
        public ActionResult Reorder(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrderViewModel OVM = new OrderViewModel();
            OVM = GenerateOrderDDL(OVM);

            OVM.Title = "Reorder";
            OVM.ORDER = db.ORDERs.Include(i => i.ORDERDETAILs).Include(i => i.CUSTOMER).Include(i => i.PHARMACY).SingleOrDefault(s => s.ORDERID == id);
            OVM.ORDER.ORDERID = 0;
            OVM.ORDER.STATUS = null;
            OVM.ORDERDETAILList = OVM.ORDER.ORDERDETAILs.ToList();

            return View("Create", OVM);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrderViewModel OVM = new OrderViewModel();
            OVM = GenerateOrderDDL(OVM);
            OVM.Title = "Edit Order";

            if (OVM == null)
            {
                return HttpNotFound();
            }

            OVM.ORDER = db.ORDERs.Include(i => i.ORDERDETAILs).Include(i => i.CUSTOMER).Include(i => i.PHARMACY).SingleOrDefault(s => s.ORDERID == id);
            OVM.ORDERDETAILList = OVM.ORDER.ORDERDETAILs.ToList();
            
            //SOURCE ORDER
            List<DropdownLIST> SL = GenerateSourceOrder();
            ViewBag.SourceOrderList = new SelectList(SL, "Value", "Text");

            //INFO DISCOUNT CUS
            List<DropdownLIST> IDCL = GenerateInfoDiscoutCust();
            ViewBag.IDCList = new SelectList(IDCL, "Value", "Text");

            //SUBMISSION OF DISCOUNTS
            List<DropdownLIST> SOD = GenerateSubmissionOfDiscount();
            ViewBag.SODList = new SelectList(SOD, "Value", "Text");

            return View("Create", OVM);
        }

        public async Task SendNotifyEmail(int oStatus, ORDER o)
        {
            string subject = "[PURCHASE ORDER #" + o.ORDERID + "] - " + Helpers.AppName;
            string ccEmail = ConfigurationManager.AppSettings["CallCenterEmail"].ToString();
            string sanofiEmail = ConfigurationManager.AppSettings["SanofiEmail"].ToString();

            if (o.CUSTOMERID == 0)
            {
                o.CUSTOMER = db.CUSTOMERs.Find(0);
            }

            string bodyCustomer = MailTemplate.ConstructOrderNotification(new EmailRecipient { Name = o.CUSTOMER.FULLNAME, ORDER = o, ORDERDETAILList = o.ORDERDETAILs.ToList(), RecipientType = RecipientType.Customer });
            string bodyDistributor = MailTemplate.ConstructOrderNotification(new EmailRecipient { Name = o.DISTRIBUTORNAME, ORDER = o, ORDERDETAILList = o.ORDERDETAILs.ToList(), RecipientType = RecipientType.Distributor });
            string bodyPharmacy = MailTemplate.ConstructOrderNotification(new EmailRecipient { Name = o.PHARMACY.PHARMACYNAME, ORDER = o, ORDERDETAILList = o.ORDERDETAILs.ToList(), RecipientType = RecipientType.Pharmacy });

            List<string> bcc = new List<string>(new string[] { sanofiEmail });

            if (oStatus == (int)OrderStatus.Submitted)
            {
                List<string> to = new List<string>(new string[] { o.DISTRIBUTOREMAIL, ccEmail });

                // TODO: if use toList, please add Helpers.AppName for from recipient displayName
                MailMessage mailDistributor = Mailer.ConstructMailMessage(to, null, bcc, subject, bodyDistributor, true, Helpers.AppName);

                await Task.WhenAll(
                    Mailer.SmtpSendAsync(mailDistributor)
                );
            }
            else if (oStatus == (int)OrderStatus.Confirmed || oStatus == (int)OrderStatus.Rejected)
            {
                MailMessage mailCustomer = Mailer.ConstructMailMessage(o.CUSTOMER.EMAIL, null, null, subject, bodyCustomer, true);
                MailMessage mailPharmacy = Mailer.ConstructMailMessage(o.PHARMACY.EMAIL, null, null, subject, bodyPharmacy, true);

                await Task.WhenAll(
                    Mailer.SmtpSendAsync(mailCustomer),
                    Mailer.SmtpSendAsync(mailPharmacy)
                );
            }
            else if (oStatus == (int)OrderStatus.Complete)
            {
                MailMessage mailDistributor = Mailer.ConstructMailMessage(o.DISTRIBUTOREMAIL, null, null, subject, bodyDistributor, true);

                MailMessage mailCustomer = Mailer.ConstructMailMessage(o.CUSTOMER.EMAIL, null, null, subject, bodyCustomer, true);
                MailMessage mailPharmacy = Mailer.ConstructMailMessage(o.PHARMACY.EMAIL, null, null, subject, bodyPharmacy, true);

                await Task.WhenAll(
                    Mailer.SmtpSendAsync(mailDistributor),
                    Mailer.SmtpSendAsync(mailCustomer),
                    Mailer.SmtpSendAsync(mailPharmacy)
                );
            }
        }

        public ActionResult Create()
        {
            OrderViewModel OVM = new OrderViewModel();
            OVM = GenerateOrderDDL(OVM);
            OVM.Title = "Create New Order";

            //SOURCE ORDER
            List<DropdownLIST> SL = GenerateSourceOrder();
            ViewBag.SourceOrderList = new SelectList(SL, "Value", "Text");

            //INFO DISCOUNT CUS
            List<DropdownLIST> IDCL = GenerateInfoDiscoutCust();
            ViewBag.IDCList = new SelectList(IDCL, "Value", "Text");

            //SUBMISSION OF DISCOUNTS
            List<DropdownLIST> SOD = GenerateSubmissionOfDiscount();
            ViewBag.SODList = new SelectList(SOD, "Value", "Text");
            
            return View(OVM);
        }

        private List<DropdownLIST> GenerateSourceOrder()
        {
            List<DropdownLIST> SL = new List<DropdownLIST>();
            SL.Add(new DropdownLIST { Text = "Back Order", Value = "Back Order" });
            SL.Add(new DropdownLIST { Text = "Customer Call", Value = "Customer Call" });
            SL.Add(new DropdownLIST { Text = "Customer Email", Value = "Customer Email" });
            SL.Add(new DropdownLIST { Text = "Customer Whatsapp", Value = "Customer Whatsapp" });
            SL.Add(new DropdownLIST { Text = "Internal APL", Value = "Internal APL" });
            SL.Add(new DropdownLIST { Text = "Sanofi Call", Value = "Sanofi Call" });
            SL.Add(new DropdownLIST { Text = "Sanofi Email", Value = "Sanofi Email" });
            SL.Add(new DropdownLIST { Text = "Sanofi Whatsapp", Value = "Sanofi Whatsapp" });
            SL.Add(new DropdownLIST { Text = "Other", Value = "Other" });

            return SL;
        }

        private List<DropdownLIST> GenerateInfoDiscoutCust()
        {
            List<DropdownLIST> IDCL = new List<DropdownLIST>();
            IDCL.Add(new DropdownLIST { Text = "ASDP", Value = "ASDP" });
            IDCL.Add(new DropdownLIST { Text = "DPL", Value = "DPL" });
            IDCL.Add(new DropdownLIST { Text = "Netto", Value = "Netto" });

            return IDCL;
        }

        private List<DropdownLIST> GenerateSubmissionOfDiscount()
        {
            List<DropdownLIST> SOD = new List<DropdownLIST>();
            SOD.Add(new DropdownLIST { Text = "CallCenter", Value = "CallCenter" });
            SOD.Add(new DropdownLIST { Text = "iDiscount", Value = "iDiscount" });

            return SOD;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(OrderViewModel OVM)
        {
            //SOURCE ORDER
            List<DropdownLIST> SL = GenerateSourceOrder();
            ViewBag.SourceOrderList = new SelectList(SL, "Value", "Text");

            //INFO DISCOUNT CUS
            List<DropdownLIST> IDCL = GenerateInfoDiscoutCust();
            ViewBag.IDCList = new SelectList(IDCL, "Value", "Text");

            //SUBMISSION OF DISCOUNTS
            List<DropdownLIST> SOD = GenerateSubmissionOfDiscount();
            ViewBag.SODList = new SelectList(SOD, "Value", "Text");
            OVM = GenerateOrderDDL(OVM);

            ModelState.RemoveFor<OrderViewModel>(x => x.ORDERDETAILList.Single().PRODUCT);
            if (OVM.ORDERDETAILList != null)
            {
                int ODL = OVM.ORDERDETAILList.Count();
                for (int i = OVM.ORDERDETAILList.Count - 1; i >= 0; i--)
                {
                    if (OVM.ORDERDETAILList[i].PRODUCTID == 0)
                    {
                        OVM.ORDERDETAILList.Remove(OVM.ORDERDETAILList[i]);
                        ModelState.RemoveFor<OrderViewModel>(r => r.ORDERDETAILList[i]);
                    }
                }
            }

            if (OVM.SubmitType == (int)OrderStatus.Submitted)
            {
                //if((OVM.ChannelType != ChannelType.Hospital.Value && OVM.ChannelType != ChannelType.Pharmacy.Value) && OVM.ORDER.CUSTOMERID == 0)
                //{
                //    ModelState.AddModelError("ORDER.CUSTOMERID", "Customer is required");
                //}
                if (OVM.ORDER.DELIVERYADDRESS == null)
                {
                    ModelState.AddModelError("ORDER.DELIVERYADDRESS", "Delivery Address is required");
                }
                if (OVM.ORDER.DELIVERYDATE == null)
                {
                    ModelState.AddModelError("ORDER.DELIVERYDATE", "Delivery Date is required");
                }
                if (OVM.ORDER.DISTRIBUTORNAME == "" || OVM.ORDER.DISTRIBUTORNAME == null)
                {
                    ModelState.AddModelError("ORDER.DISTRIBUTORNAME", "Distributor Name is required");
                }
                if (OVM.ORDER.DISTRIBUTORPHONENUMBER == "" || OVM.ORDER.DISTRIBUTORPHONENUMBER == null)
                {
                    ModelState.AddModelError("ORDER.DISTRIBUTORPHONENUMBER", "Distributor Phone is required");
                }
                if (OVM.ORDER.DISTRIBUTOREMAIL == "" || OVM.ORDER.DISTRIBUTOREMAIL == null)
                {
                    ModelState.AddModelError("ORDER.DISTRIBUTOREMAIL", "Distributor Email is required");
                }

                if (OVM.ORDERDETAILList == null || OVM.ORDERDETAILList.Count() < 1)
                {
                    ModelState.AddModelError("ORDERDETAILList", "Please select at least one product");
                }
                else
                {

                }
            }

            if (ModelState.IsValid)
            {
                OVM.ORDER.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                OVM.ORDER.CREATEDDATE = DateTime.Now;
                OVM.ORDER.STATUS = OVM.SubmitType;

                if (OVM.SubmitType == (int)OrderStatus.Inquiry)
                {
                    OVM.ORDER.INQUIRYNOTES = OVM.InquiryNotes;
                    OVM.ORDER.INQUIRYTYPE = OVM.InquiryType;
                }
               
                using (var dbTran = db.Database.BeginTransaction())
                {
                    int orderId = 0;

                    try
                    {
                        if (OVM.ORDER.ORDERID == 0)
                        {
                            db.ORDERs.Add(OVM.ORDER);
                        }
                        else
                        {
                            db.Entry<ORDER>(OVM.ORDER).State = EntityState.Modified;
                            db.SaveChanges();

                            List<ORDERDETAIL> ODDelete = db.ORDERDETAILs.Where(w => w.ORDERID == OVM.ORDER.ORDERID).ToList();
                            db.ORDERDETAILs.RemoveRange(ODDelete);
                        }

                        db.SaveChanges();

                        orderId = OVM.ORDER.ORDERID;

                        if (OVM.ORDERDETAILList != null)
                        {
                            foreach (ORDERDETAIL od in OVM.ORDERDETAILList)
                            {
                                od.ORDERID = orderId;

                                od.PRODUCT = null;
                                db.ORDERDETAILs.Add(od);
                            }
                            db.SaveChanges();
                        }

                        dbTran.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbTran.Rollback();
                        LogSystem.ErrorLog(ex, null);
                        ViewBag.Error = "Saving Order Failed";

                        //throw;
                        return View(OVM);
                    }

                    if (OVM.SubmitType == (int)OrderStatus.Submitted || OVM.SubmitType == (int)OrderStatus.Confirmed)
                    {
                        ORDER o = db.ORDERs.Find(orderId);

                        await SendNotifyEmail(OVM.SubmitType, o);
                    }

                }

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Error = "Please check form below";
            }

            return View(OVM);
        }

        [HttpPost]
        public ActionResult LoadOrder()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var from = Request.Form.GetValues("date_from").FirstOrDefault();
            var to = Request.Form.GetValues("date_to").FirstOrDefault();

            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //Find Search Keyword
            var searchKey = Request.Form.GetValues("search[value]").FirstOrDefault();

            //Find Individual Search Keyword
            //var indSearchKey = Request.Form.GetValues("columns").ToList();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            // dc.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
            
            List<OrderIndexViewModel> v = (from o in db.ORDERs
                                           join od in db.ORDERDETAILs
                                           on o.ORDERID equals od.ORDERID into JoinedEmpDept
                                           from od in JoinedEmpDept.DefaultIfEmpty()
                                           select new OrderIndexViewModel
                                           {
                                               ORDER = o,
                                               ORDERDETAIL = od
                                           }).AsEnumerable().Select(s => new OrderIndexViewModel
                                           {
                                               S_ORDERID             = s.ORDER.ORDERID,
                                               S_BATCHNO             = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.BATCHNO : " - ",
                                               I_CREATEDBY           = s.ORDER.CREATEDBY, 
                                               S_CREATEDBY           = s.ORDER.USER.USERNAME,
                                               S_CITY                = s.ORDER.CITY1 != null ? s.ORDER.CITY1.CITY1 : " - ",
                                               S_CUST_CODE           = s.ORDER.CUSTOMER != null ? s.ORDER.CUSTOMER.CUSTOMERCODE : " - ",
                                               S_CUST_NAME           = s.ORDER.CUSTOMER != null ? s.ORDER.CUSTOMER.FULLNAME : " - ",
                                               S_CUST_PHONE          = s.ORDER.CUSTOMER != null ? s.ORDER.CUSTOMER.PHONENUMBER : " - ",
                                               S_DELIV_ADDRES        = s.ORDER.DELIVERYADDRESS,
                                               S_DELIV_DATE          = s.ORDER.DELIVERYDATE.HasValue ? s.ORDER.DELIVERYDATE.Value.ToString("dd/MM/yyyy") : " - ",
                                               S_DISCOUNT            = s.ORDERDETAIL != null ? s.ORDERDETAIL.DISCOUNT.ToString() : " - ",
                                               S_DIST_EMAIL          = s.ORDER.DISTRIBUTOREMAIL,
                                               S_DIST_NAME           = s.ORDER.DISTRIBUTORNAME,
                                               S_DIST_PHONE          = s.ORDER.DISTRIBUTORPHONENUMBER,
                                               S_PHAR_ADDRESS        = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.ADDRESS : " - ",
                                               S_PHAR_CODE           = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.PHARMACYCODE : " - ",
                                               S_PHAR_EMAIL          = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.EMAIL : " - ",
                                               S_PHAR_NAME           = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.PHARMACYNAME : " - ",
                                               S_PHAR_PHONE          = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.PHONENUMBER : " - ",
                                               S_PLANT               = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PLANT : " - ",
                                               S_PRICE               = s.ORDERDETAIL != null ? "Rp " + String.Format("{0:#,#}", s.ORDERDETAIL.PRICE) : " - ",
                                               S_PROD_CODE           = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PRODUCTCODE : " - ",
                                               S_PROD_NAME           = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PRODUCTNAME : " - ",
                                               S_QTY                 = s.ORDERDETAIL != null ? s.ORDERDETAIL.QTY.ToString() : " - ",
                                               S_UOM                 = s.ORDERDETAIL != null ? s.ORDERDETAIL.UOM : " - ",
                                               S_EXPDATE             = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.EXPIREDDATE.Value.ToString("dd MMM yyyy") : " - ",
                                               S_CREATEDDATE         = s.ORDER.CREATEDDATE.ToString("dd/MM/yyyy H:mm:ss"),
                                               O_CREATEDDATE         = s.ORDER.CREATEDDATE.ToString("yyyyMMddHmmss"),
                                               D_CREATEDDATE         = s.ORDER.CREATEDDATE,
                                               S_STATUS              = ((OrderStatus)s.ORDER.STATUS).GetDescription(),
                                               I_STATUS              = s.ORDER.STATUS.Value.ToString(),
                                               SOURCEORDER           = s.ORDER.SOURCEORDER,
                                               INFODISCCUSTOMER      = s.ORDERDETAIL != null ? s.ORDERDETAIL.INFODISCCUSTOMER : " - ",
                                               SUBMISSIONOFDISCOUNTS = s.ORDERDETAIL != null ? s.ORDERDETAIL.SUBMISSIONOFDISCOUNTS : " - ",
                                               DISCOUNTREFERENCE     = s.ORDERDETAIL != null ? s.ORDERDETAIL.DISCOUNTREFERENCE : " - ",
                                               USERCHANNEL           = s.ORDER.USERCHANNEL != null ? s.ORDER.USERCHANNEL : " - "
                                           }).ToList();

            //FILTER
            //if (!string.IsNullOrEmpty(searchKey))
            //{
            //    v = v.Where(
            //            w =>
            //            w.ORDER.ORDERID.Equals(searchKey)
            //            || w.CUSTOMER.FULLNAME.Contains(searchKey)
            //            || w.PHARMACY.PHARMACYNAME.Contains(searchKey)
            //            || w.PRODUCT.PRODUCTNAME.Contains(searchKey)
            //            || w.S_CREATEDDATE.Contains(searchKey)
            //            || w.ORDER.CITY.Contains(searchKey)
            //            || w.ORDER.STATUS.Equals(searchKey)
            //        ).ToList();
            //}

            //DATE RANGE FILTER
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                v = v.Where("D_CREATEDDATE >= @0 && D_CREATEDDATE <= @1", Convert.ToDateTime(from + " 00:00"), Convert.ToDateTime(to + " 23:59")).ToList();
            }

            //INDIVIDUAL SEARCH
            foreach (string key in Request.Form)
            {
                if (key.EndsWith("[search][value]") && !string.IsNullOrEmpty(Request.Form.GetValues(key).FirstOrDefault()))
                {
                    string col = key.Substring(0, key.IndexOf("[search][value]"));
                    string value = Request.Form.GetValues(col + "[search][value]").FirstOrDefault().ToLower();
                    string colName = Request.Form.GetValues(col + "[name]").FirstOrDefault();
                    bool searchable = Convert.ToBoolean(Request.Form.GetValues(col + "[searchable]").FirstOrDefault());

                    if (searchable)
                    {
                        var pi = typeof(OrderIndexViewModel).GetProperty(colName);

                        if (pi.PropertyType == typeof(Int32))
                        {
                            v = v.Where(colName + "==" + value).ToList();
                        }
                        else
                        {
                            v = v.Where(colName + " != null && " + colName + ".ToLower().Contains(@0)", value).ToList();
                        }
                        //v = v.Where(w => pi.GetValue(w, null) == value).ToList();
                    }
                }
            }

            // Filter Order by UserLogin
            if ((UserRole)Session["UserRole"] == UserRole.ContactCenter)
            {
                int iUserID = Convert.ToInt32(Session["UserID"].ToString());
                v = v.Where(w => w.I_CREATEDBY == iUserID).ToList();
            }

            //SORT
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                sortColumn = sortColumn == "S_CREATEDDATE" ? "O_CREATEDDATE" : sortColumn;
                var pi = typeof(OrderIndexViewModel).GetProperty(sortColumn);

                if (pi != null)
                {
                    if (sortColumnDir == "asc")
                    {
                        v = v.OrderBy(x => pi.GetValue(x, null)).ToList();
                    }
                    else
                    {
                        v = v.OrderByDescending(x => pi.GetValue(x, null)).ToList();
                    }
                }
            }

            recordsTotal = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ORDER oRDER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oRDER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUSTOMERID = new SelectList(db.CUSTOMERs, "CUSTOMERID", "CUSTOMERCODE", oRDER.CUSTOMERID);
            ViewBag.PHARMACYID = new SelectList(db.PHARMACies, "PHARMACYID", "PHARMACYCODE", oRDER.PHARMACYID);
            ViewBag.CREATEDBY = new SelectList(db.USERs, "USERID", "USERNAME", oRDER.CREATEDBY);
            return View(oRDER);
        }

        //// GET: Order/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ORDER oRDER = db.ORDERs.Find(id);
        //    if (oRDER == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(oRDER);
        //}

        //// POST: Order/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ORDER oRDER = db.ORDERs.Find(id);
        //    db.ORDERs.Remove(oRDER);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        private OrderViewModel GenerateOrderDDL(OrderViewModel OVM)
        {
            OVM.CustList = db.CUSTOMERs.Where(w => w.STATUS == (int)Status.Active || w.STATUS == (int)Status.Approval).ToList();
            OVM.PharList = db.PHARMACies.Where(w => w.STATUS == (int)Status.Active).ToList();
            OVM.ProdList = db.PRODUCTs.Where(w => w.STATUS == (int)Status.Active).ToList();
            //OVM.Cities   = Helpers.GetCityList();
            OVM.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            return OVM;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Export(string date_from, string date_to)
        {
            IQueryable<OrderIndexViewModel> orderList;
            
            var dateFrom = !String.IsNullOrEmpty(date_from) ? Convert.ToDateTime(date_from + " 00:00") : DateTime.MinValue;
            var dateTo = !String.IsNullOrEmpty(date_to) ? Convert.ToDateTime(date_to + " 23:59") : DateTime.MaxValue;

            if ((UserRole)Session["UserRole"] == UserRole.ContactCenter)
            {
                int iUserID = Convert.ToInt32(Session["UserID"].ToString());
                
                orderList = (from o in db.ORDERs
                             join od in db.ORDERDETAILs
                             on o.ORDERID equals od.ORDERID into JoinedEmpDept
                             from od in JoinedEmpDept.DefaultIfEmpty()
                             where o.CREATEDBY == iUserID
                                && (o.CREATEDDATE >= dateFrom && o.CREATEDDATE <= dateTo)
                             select new OrderIndexViewModel
                             {
                                 ORDER = o,
                                 ORDERDETAIL = od
                             });
            } else {
                orderList = (from o in db.ORDERs
                             join od in db.ORDERDETAILs
                             on o.ORDERID equals od.ORDERID into JoinedEmpDept
                             from od in JoinedEmpDept.DefaultIfEmpty()
                             where (o.CREATEDDATE >= dateFrom && o.CREATEDDATE <= dateTo)
                             select new OrderIndexViewModel
                             {
                                 ORDER = o,
                                 ORDERDETAIL = od
                             });
            }
            
            List<OrderExport> v = orderList.AsEnumerable().Select(s => new OrderExport
                                   {
                                       ORDER_ID              = s.ORDER.ORDERID.ToString(),
                                       BATCHNO               = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.BATCHNO : " - ",
                                       CREATED_BY            = s.ORDER.USER.USERNAME,
                                       CITY                  = s.ORDER.CITY1 != null ? s.ORDER.CITY1.CITY1 : " - ",
                                       CUSTOMER_CODE         = s.ORDER.CUSTOMER != null ? "=\"" + s.ORDER.CUSTOMER.CUSTOMERCODE + "\"" : " - ",
                                       CUSTOMER_NAME         = s.ORDER.CUSTOMER != null ? s.ORDER.CUSTOMER.FULLNAME : " - ",
                                       CUSTOMER_PHONE        = s.ORDER.CUSTOMER != null ? "=\"" + s.ORDER.CUSTOMER.PHONENUMBER + "\"" : " - ",
                                       DELIVERY_ADDRESS      = s.ORDER.DELIVERYADDRESS,
                                       DELIVERY_DATE         = s.ORDER.DELIVERYDATE.HasValue ? s.ORDER.DELIVERYDATE.Value.ToString("dd MMMM yyyy") : " - ",
                                       DISCOUNT              = s.ORDERDETAIL != null && s.ORDERDETAIL.DISCOUNT.HasValue ? (s.ORDERDETAIL.DISCOUNT.ToString() != "" ? s.ORDERDETAIL.DISCOUNT.ToString().Replace(".", ",") : "0") : "0",
                                       DISCOUNT_CUSTOMER     = s.ORDERDETAIL != null && s.ORDERDETAIL.DISCOUNT_CUST.HasValue ? (s.ORDERDETAIL.DISCOUNT_CUST.ToString() != "" ? s.ORDERDETAIL.DISCOUNT_CUST.ToString().Replace(".", ",") : "0") : "0",
                                       DISTRIBUTOR_EMAIL     = s.ORDER.DISTRIBUTOREMAIL,
                                       DISTRIBUTOR_NAME      = s.ORDER.DISTRIBUTORNAME,
                                       DISTRIBUTOR_PHONE     = "=\"" + s.ORDER.DISTRIBUTORPHONENUMBER + "\"",
                                       PHARMACY_ADDRESS      = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.ADDRESS : " - ",
                                       PHARMACY_CODE         = s.ORDER.PHARMACY != null ? "=\"" + s.ORDER.PHARMACY.PHARMACYCODE + "\"" : " - ",
                                       PHARMACY_EMAIL        = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.EMAIL : " - ",
                                       PHARMACY_NAME         = s.ORDER.PHARMACY != null ? s.ORDER.PHARMACY.PHARMACYNAME : " - ",
                                       PHARMACY_PHONE        = s.ORDER.PHARMACY != null ? "=\"" + s.ORDER.PHARMACY.PHONENUMBER + "\"" : " - ",
                                       PLANT                 = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PLANT : " - ",
                                       PRICE                 = s.ORDERDETAIL != null && s.ORDERDETAIL.PRICE.HasValue ? s.ORDERDETAIL.PRICE.ToString() : " - ",
                                       PRODUCT_CODE          = s.ORDERDETAIL != null ? "=\"" + s.ORDERDETAIL.PRODUCT.PRODUCTCODE + "\"" : " - ",
                                       PRODUCT_NAME          = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PRODUCTNAME : " - ",
                                       QTY                   = s.ORDERDETAIL != null && s.ORDERDETAIL.QTY.HasValue ? s.ORDERDETAIL.QTY.ToString() : " - ",
                                       UOM                   = s.ORDERDETAIL != null ? s.ORDERDETAIL.UOM : " - ",
                                       EXPDATE               = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.EXPIREDDATE.Value.ToString("dd MMMM yyyy") : " - ",
                                       ORDER_DATE            = s.ORDER.CREATEDDATE.ToString("dd MMMM yyyy H:mm:ss"),
                                       STATUS                = ((OrderStatus)s.ORDER.STATUS).GetDescription(),
                                       REJECT_REASON         = s.ORDER.REJECTREASON,
                                       INQUIRY_NOTES         = s.ORDER.INQUIRYNOTES,
                                       INQUIRY_TYPE          = s.ORDER.INQUIRYTYPE,
                                       SOURCEORDER           = s.ORDER.SOURCEORDER,
                                       INFODISCCUSTOMER      = s.ORDERDETAIL != null ? s.ORDERDETAIL.INFODISCCUSTOMER : " - ",
                                       SUBMISSIONOFDISCOUNTS = s.ORDERDETAIL != null ? s.ORDERDETAIL.SUBMISSIONOFDISCOUNTS : " - ",
                                       DISCOUNTREFERENCE     = s.ORDERDETAIL != null ? s.ORDERDETAIL.DISCOUNTREFERENCE : " - ",
                                       USERCHANNEL           = s.ORDER.USERCHANNEL != null ? s.ORDER.USERCHANNEL : " - ",
                                       SO                    = s.ORDER.SO,
                                       DO                    = s.ORDER.DO,
                                       UPDATESODO            = s.ORDER.USER1 != null ? s.ORDER.USER1.FULLNAME : "-",
                                       GROSS_SALES           = s.ORDERDETAIL != null && s.ORDERDETAIL.PRICE.HasValue && s.ORDERDETAIL.QTY.HasValue ? (s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY).ToString() : "0",
                                       NETT_SALES            = s.ORDERDETAIL != null && s.ORDERDETAIL.PRICE.HasValue && s.ORDERDETAIL.QTY.HasValue && s.ORDERDETAIL.DISCOUNT.HasValue && s.ORDERDETAIL.DISCOUNT_CUST.HasValue ? ((double)((s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY) - ((double)(s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY) * (double)((s.ORDERDETAIL.DISCOUNT ?? 0) + (s.ORDERDETAIL.DISCOUNT_CUST ?? 0)) / 100))).ToString().Replace(".", ",") : "0"
            }).ToList();

            string filename = "[Contact Center] Transaction Report_" + DateTime.Now.ToString("yyyyMMdd");

            if(!String.IsNullOrEmpty(date_from) && !String.IsNullOrEmpty(date_to))
            {
                filename = "[Contact Center] Transaction Report (Period " + dateFrom.ToString("dd-MM-yyyy") + " - " + dateTo.ToString("dd-MM-yyyy") + ")";
            }

            var gv = new GridView();
            gv.DataSource = v;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }

        public void NewExport(string date_from, string date_to)
        {

            var dateFrom = !String.IsNullOrEmpty(date_from) ? Convert.ToDateTime(date_from + " 00:00") : DateTime.MinValue;
            var dateTo = !String.IsNullOrEmpty(date_to) ? Convert.ToDateTime(date_to + " 23:59") : DateTime.MaxValue;

            var callback = from cb in db.CALLBACKs
                           group cb by cb.ORDERID into g
                           select new
                           {
                               orderid = g.Key,
                               CNT = g.Count(),
                           };

            List<NewOrderExport> v = (from or in db.ORDERs
                                      join od in db.ORDERDETAILs on or.ORDERID equals od.ORDERID into join1
                                      from x in join1.DefaultIfEmpty()
                                      join cb in callback on or.ORDERID equals cb.orderid into join2
                                      from y in join2.DefaultIfEmpty()
                                      where (or.CREATEDDATE >= dateFrom && or.CREATEDDATE <= dateTo)
                                      select new OrderIndexViewModel
                                      {
                                          ORDER = or,
                                          ORDERDETAIL = x,
                                          CNT = y.CNT
                                      }).AsEnumerable().Select(s => new NewOrderExport
                                      {
                                          TANGGAL         = s.ORDER.CREATEDDATE.ToString(),
                                          NAMA_TELESELLER = s.ORDER.USER.USERNAME,
                                          SUMBER_ORDER    = s.ORDER.DISTRIBUTORNAME,
                                          CABANG_APL      = s.ORDER.CITY1 != null ? s.ORDER.CITY1.CITY1 : " - ",
                                          KODE_CUSTOMER   = s.ORDER.CUSTOMER != null ? "=\"" + s.ORDER.CUSTOMER.CUSTOMERCODE + "\"" : " - ",
                                          NAMA_CUSTOMER   = s.ORDER.CUSTOMER != null ? s.ORDER.CUSTOMER.FULLNAME : " - ",
                                          USER            = s.ORDER.USERCHANNEL != null ? s.ORDER.USERCHANNEL : " - ",
                                          PRODUK          = s.ORDERDETAIL != null ? s.ORDERDETAIL.PRODUCT.PRODUCTNAME : " - ",
                                          QTY             = s.ORDERDETAIL != null && s.ORDERDETAIL.QTY.HasValue ? s.ORDERDETAIL.QTY.ToString() : " - ",
                                          DISKON_CUSTOMER = s.ORDERDETAIL != null && s.ORDERDETAIL.DISCOUNT_CUST.HasValue ? (s.ORDERDETAIL.DISCOUNT_CUST.ToString() != "" ? s.ORDERDETAIL.DISCOUNT_CUST.ToString().Replace(".", ",") : "0") : "0",
                                          KET_DISC_CUS    = s.ORDERDETAIL != null ? s.ORDERDETAIL.INFODISCCUSTOMER : " - ",
                                          DISC_PANEL      = s.ORDERDETAIL != null && s.ORDERDETAIL.DISCOUNT.HasValue ? (s.ORDERDETAIL.DISCOUNT.ToString() != "" ? s.ORDERDETAIL.DISCOUNT.ToString().Replace(".", ",") : "0") : "0",
                                          PENGAJUAN_DISC  = s.ORDERDETAIL != null ? s.ORDERDETAIL.SUBMISSIONOFDISCOUNTS : " - ",
                                          SO              = s.ORDER.SO,
                                          DO              = s.ORDER.DO,
                                          STATUS          = ((OrderStatus)s.ORDER.STATUS).GetDescription(),
                                          ISSUE           = s.ORDER.REJECTREASON,
                                          CALLBACK_KE_CUS = s.CNT > 0 ? "V" : "",
                                          GROSS_SALES     = s.ORDERDETAIL != null && s.ORDERDETAIL.PRICE.HasValue && s.ORDERDETAIL.QTY.HasValue ? (s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY).ToString() : "0",
                                          NETT_SALES      = s.ORDERDETAIL != null && s.ORDERDETAIL.PRICE.HasValue && s.ORDERDETAIL.QTY.HasValue && s.ORDERDETAIL.DISCOUNT_CUST.HasValue && s.ORDERDETAIL.DISCOUNT.HasValue ? ((double)((s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY) - ((double)(s.ORDERDETAIL.PRICE * s.ORDERDETAIL.QTY) * (double)((s.ORDERDETAIL.DISCOUNT ?? 0) + (s.ORDERDETAIL.DISCOUNT_CUST ?? 0)) / 100))).ToString().Replace(".", ",") : "0"
                                      }).ToList();


            string filename = "[Contact Center] Admin - Transaction Report_" + DateTime.Now.ToString("yyyyMMdd");
            
            if (!String.IsNullOrEmpty(date_from) && !String.IsNullOrEmpty(date_to))
            {
                filename = "[Contact Center] Admin - Transaction Report (Period " + dateFrom.ToString("dd-MM-yyyy") + " - " + dateTo.ToString("dd-MM-yyyy") + ")";
            }

            var gv = new GridView();
            gv.DataSource = v;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }

        [HttpPost]
        public async Task<ActionResult> CBinternal(int orderid)
        {

            var json = Json(null);
            try
            {
                CALLBACK cALLBACK = new CALLBACK();
                cALLBACK.ORDERID = orderid;
                cALLBACK.USERID = Convert.ToInt16(Session["UserID"].ToString());
                cALLBACK.TYPE = "INTERNAL";
                cALLBACK.CREATEDDATE = DateTime.Now;


                db.CALLBACKs.Add(cALLBACK);
                await db.SaveChangesAsync();
                json = Json(new { success = true });
            }
            catch
            {
                json = Json(new { success = false });
                throw;
            }

            return json;
        }

        [HttpPost]
        public async Task<ActionResult> CBexternal(int orderid)
        {
            var json = Json(null);
            try
            {
                CALLBACK cALLBACK = new CALLBACK();
                cALLBACK.ORDERID = orderid;
                cALLBACK.USERID = Convert.ToInt16(Session["UserID"].ToString());
                cALLBACK.TYPE = "EXTERNAL";
                cALLBACK.CREATEDDATE = DateTime.Now;


                db.CALLBACKs.Add(cALLBACK);
                await db.SaveChangesAsync();
                json = Json(new { success = true });
            }
            catch
            {
                json = Json(new { success = false });
                throw;
            }

            return json;
        }

        [HttpPost]
        public async Task<ActionResult> UpdateDetail(int orderid, string DO, string SO)
        {
            var json = Json(null);
            try
            {
                ORDER oRDER = db.ORDERs.Find(orderid);
                oRDER.SO = SO;
                oRDER.DO = DO;
                oRDER.UPDATESODO = Convert.ToInt16(Session["UserID"].ToString());
                db.Entry(oRDER).State = EntityState.Modified;
                db.SaveChanges();
                await db.SaveChangesAsync();
                json = Json(new { success = true });
            }
            catch
            {
                json = Json(new { success = false });
                throw;
            }

            return json;
        }

        public void ExportCallback()
        {
            List<CallbackExport> v = (from cb in db.CALLBACKs
                                      join or in db.ORDERs on cb.ORDERID equals or.ORDERID
                                      join us in db.USERs on cb.USERID equals us.USERID
                                      select new { cb, or, us }).Select(s => new CallbackExport
                                      {
                                          IDCB = s.cb.IDCB,
                                          ORDERID = s.cb.ORDERID.ToString(),
                                          NAME = s.us.FULLNAME,
                                          TYPE = s.cb.TYPE,
                                          CREATEDDATE = s.cb.CREATEDDATE.ToString()
                                      }).ToList();

            string filename = "[Contact Center] CallBack Report_" + DateTime.Now.ToString("yyyyMMdd");

            var gv = new GridView();
            gv.DataSource = v;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }
    }
}
