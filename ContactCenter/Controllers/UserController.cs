﻿using ContactCenter.Common;
using Cryptography;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactCenter.Controllers
{
    public class UserController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();
        private AES_Cryptography _crypto = new AES_Cryptography();
        
        // GET: User
        public ActionResult Login()
        {
            ViewBag.Error = null;
            
            return View();
        }
        
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost, ActionName("ChangePassword")]
        public ActionResult ChangePassPost(string oldPassword, string newPassword, string confirmPass)
        {
            if (oldPassword == "")
            {
                ModelState.AddModelError("oldPassword", "Old Password is required");
            }
            if (newPassword == "")
            {
                ModelState.AddModelError("newPassword", "New Password is required");
            }
            if (confirmPass == "")
            {
                ModelState.AddModelError("confirmPass", "Confirm Password is required");
            }

            if (ModelState.IsValid)
            {
                string username = Session["UserName"].ToString();

                USER sysuser = db.USERs
                            .Where(user => user.USERNAME == username)
                            .FirstOrDefault();

                if (_crypto.decryptString(sysuser.PASSWORD) != oldPassword)
                {
                    ViewBag.Error = "Old Password is incorrect";
                }
                else if (newPassword != confirmPass)
                {
                    ViewBag.Error = "New and Confirm Password doesn't match";
                }
                else if (newPassword == oldPassword)
                {
                    ViewBag.Error = "Old and New Password must be different";
                }
                else
                {
                    sysuser.PASSWORD = _crypto.encryptString(newPassword);

                    try
                    {
                        db.Entry(sysuser).State = EntityState.Modified;
                        db.SaveChanges();

                        ViewBag.Success = "Password changed succesfully. You can login using your new password.";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Change Password Failed";
                        LogSystem.ErrorLog(ex, null);
                        //throw;
                    }
                }
            }

            return View();
        }

        public ActionResult Logout()
        {
            ClearSession();

            return RedirectToAction("Login", "User");
        }

        public void ClearSession()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
        }

        [HttpPost, ActionName("Login")]
        public ActionResult LoginPost(string username, string password)
        {
            if (ModelState.IsValid)
            {
                USER sysuser = db.USERs
                        .Where(user => user.USERNAME == username)
                        .FirstOrDefault();

                if (sysuser != null)
                {
                    if (sysuser.STATUS != 0)
                    {
                        if (_crypto.decryptString(sysuser.PASSWORD) == password)
                        {
                            Create_Session(sysuser);

                            if(_crypto.decryptString(sysuser.PASSWORD) == UserHelper.DefaultPassword)
                            {
                                return RedirectToAction("ChangePassword", "User");
                            }

                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ViewBag.Error = "Invalid Password";
                            Session["Username"] = sysuser.USERNAME;
                        }
                    }
                    else
                    {
                        ViewBag.Error = "Your account is inactive";
                    }
                }
                else
                {
                    ViewBag.Error = "Invalid username";
                }
            }

            return View();
        }
        
        public void Create_Session(USER sysuser)
        {
            Session["UserID"]   = sysuser.USERID;
            Session["UserName"] = sysuser.USERNAME;
            Session["UserRole"] = sysuser.ROLE;
            Session["Name"]     = sysuser.FULLNAME;
            Session["Email"]    = sysuser.EMAIL;
        }
    }
}
