﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter;
using ContactCenter.Common;
using System.Data.Entity.Validation;
using Cryptography;

namespace ContactCenter.Controllers
{
    public class UserManagementController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();
        private AES_Cryptography crypto = new AES_Cryptography();

        // GET: UserManagement
        public ActionResult Index()
        {
            var uSERs = db.USERs.Include(u => u.USER2);
            return View(uSERs.ToList());
        }

        // GET: UserManagement/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER uSER = db.USERs.Find(id);
            if (uSER == null)
            {
                return HttpNotFound();
            }
            return View(uSER);
        }

        // GET: UserManagement/Create
        public ActionResult Create()
        {
            GenerateDDL();

            return View();
        }

        // POST: UserManagement/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(USER uSER)
        {
            USER chkUser = db.USERs.Where(w => w.USERNAME == uSER.USERNAME).FirstOrDefault();
            GenerateDDL();

            if (chkUser != null)
            {
                ModelState.AddModelError("USERNAME", "Username is already exists");
            }

            if (ModelState.IsValid)
            {
                uSER.PASSWORD = crypto.encryptString(UserHelper.DefaultPassword);
                uSER.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                uSER.CREATEDDATE = DateTime.Now;
                try
                {
                    db.USERs.Add(uSER);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New User Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(uSER);
                }

                return RedirectToAction("Index");
            }
            
            return View(uSER);
        }

        // GET: UserManagement/Edit/5
        public ActionResult Edit(int? id)
        {
            GenerateDDL();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USER uSER = db.USERs.Find(id);
            if (uSER == null)
            {
                return HttpNotFound();
            }

            return View(uSER);
        }

        // POST: UserManagement/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(USER uSER, bool IsReset)
        {
            GenerateDDL();

            if (ModelState.IsValid)
            {
                if (IsReset)
                    uSER.PASSWORD = crypto.encryptString(UserHelper.DefaultPassword);
                
                try
                {
                  
                    db.Entry(uSER).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update User Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(uSER);
                }

                return RedirectToAction("Index");
            }
            
            return View(uSER);
        }

        // GET: UserManagement/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    USER uSER = db.USERs.Find(id);
        //    if (uSER == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(uSER);
        //}

        // POST: UserManagement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            USER uSER = db.USERs.Find(id);
            db.USERs.Remove(uSER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void GenerateDDL()
        {
            ViewBag.Roles = EnumHelper.ToOrderedSelectList<UserRole>();
            ViewBag.Statuses = EnumHelper.ToOrderedSelectList<Status>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
