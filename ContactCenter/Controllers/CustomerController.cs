﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ContactCenter.Common;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.Entity.Validation;
using ContactCenter.Models;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Linq.Dynamic;

namespace ContactCenter.Controllers
{
    public class CustomerController : Controller
    {
        private ContactCenterEntities db = new ContactCenterEntities();

        // GET: Customer
        public ActionResult Index()
        {
            var cUSTOMERs = db.CUSTOMERs.Include(c => c.USER).Where(w => w.CUSTOMERID != 0);
            return View(cUSTOMERs.ToList());
        }

        public ActionResult Approval()
        {
            var cUSTOMERs = db.CUSTOMERs.Include(c => c.USER).Where(w => w.CUSTOMERID != 0 && w.STATUS == (int)Status.Approval);
            if (TempData["Error"] != null)
                ViewBag.Error = TempData["Error"].ToString();

            return View(cUSTOMERs.ToList());
        }

        public ActionResult Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CUSTOMER cUSTOMER = new CUSTOMER();

            cUSTOMER = db.CUSTOMERs.Find(id);
            cUSTOMER.STATUS = (int)Status.Active;

            try
            {
                db.Entry(cUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                TempData["Error"] = "Customer Approval Failed! Please complete Customer Information first.";

                return RedirectToAction("Approval");
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                TempData["Error"] = "Customer Approval Failed!";

                return RedirectToAction("Approval");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Reject(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CUSTOMER cUSTOMER = new CUSTOMER();

            cUSTOMER = db.CUSTOMERs.Find(id);
            cUSTOMER.STATUS = (int)Status.Reject;

            try
            {
                db.Entry(cUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                TempData["Error"] = "Customer Reject Failed! Please complete Customer Information first.";

                return RedirectToAction("Approval");
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                TempData["Error"] = "Customer Reject Failed!";

                return RedirectToAction("Approval");
            }

            return RedirectToAction("Index");
        }



        public ActionResult Download()
        {
            string filename = "Upload Customer Template.xlsx";
            string file = Server.MapPath("~/Content/Templates/" + filename);
            string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(file, contentType, Path.GetFileName(file));
        }

        [HttpGet]
        public ActionResult Import()
        {
            var cUSTOMERs = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);
            return View("Index", cUSTOMERs.ToList());
        }

        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            //LogSystem logging = new LogSystem();
            try
            {
                if (excelfile == null || excelfile.ContentLength == 0)
                {
                    ViewBag.Error = "Please select an Excel File";

                    var cUSTOMERs = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);
                    return View("Index", cUSTOMERs.ToList());
                }
                else
                {
                    if (excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx") || excelfile.FileName.EndsWith("xlsb"))
                    {
                        //string consString = ConfigurationManager.ConnectionStrings["SQLConnect1"].ConnectionString;
                        //SQLConnect connSQL = new SQLConnect(consString);

                        string fileName = "";
                        if (excelfile.FileName.Contains('/'))
                        {
                            string[] aa = excelfile.FileName.Split('/');
                            fileName = aa[aa.Length - 1];
                        }
                        else
                        {
                            if (excelfile.FileName.Contains('\\'))
                            {
                                string[] aa = excelfile.FileName.Split('\\');
                                fileName = aa[aa.Length - 1];
                            }
                            else
                            {
                                fileName = excelfile.FileName;
                            }
                        }
                        string extension = System.IO.Path.GetExtension(excelfile.FileName);
                        string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/UploadedFolder"), fileName);

                        if (System.IO.File.Exists(path1))
                            System.IO.File.Delete(path1);

                        excelfile.SaveAs(path1);

                        string excelConnectionString = string.Empty;

                        excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                        //OleDbConnection conn = new OleDbConnection(excelConnectionString);

                        using (OleDbConnection conn = new OleDbConnection(excelConnectionString))
                        {
                            try
                            {
                                if (conn.State == ConnectionState.Closed)
                                    conn.Open();

                                DataTable dtExcelSchema;
                                dtExcelSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                                string sheetName = "Sheet1";
                                string query = "SELECT * FROM [" + sheetName + "$]";

                                OleDbCommand cmd = new OleDbCommand(query, conn);
                                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                                DataSet ds = new DataSet();

                                da.Fill(ds);
                                DateTime today = DateTime.Now;

                                // validate upload
                                if (!Validate(ds.Tables[0]))
                                {
                                    ViewBag.Error = "See error message below";

                                    var customers = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);
                                    return View("Index", customers.ToList());
                                }

                                int val = 0;

                                using (var dbTran = db.Database.BeginTransaction())
                                {
                                    var cUSTOMER = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);

                                    try
                                    {
                                        foreach (DataRow dr in ds.Tables[0].Rows)
                                        {
                                            string customer_code = dr["CUSTOMER_CODE"].ToString().Trim().TrimStart().TrimEnd();
                                            string customer_name = dr["FULLNAME"].ToString().Trim().TrimStart().TrimEnd();
                                            string gender = dr["GENDER"].ToString().Trim().TrimStart().TrimEnd();
                                            string phone_number = dr["PHONE_NUMBER"].ToString().Trim().TrimStart().TrimEnd();
                                            string phone_number2 = dr["PHONE_NUMBER2"].ToString().Trim().TrimStart().TrimEnd();
                                            string email = dr["EMAIL"].ToString().Trim().TrimStart().TrimEnd();
                                            string city = dr["CITY"].ToString().Trim().TrimStart().TrimEnd();
                                            string address = dr["ADDRESS"].ToString().Trim().TrimStart().TrimEnd();
                                            string pop1 = dr["PLACE_OF_PRACTICE1"].ToString().Trim().TrimStart().TrimEnd();
                                            string pop2 = dr["PLACE_OF_PRACTICE2"].ToString().Trim().TrimStart().TrimEnd();
                                            string pop3 = dr["PLACE_OF_PRACTICE3"].ToString().Trim().TrimStart().TrimEnd();

                                            int i_city = db.CITies.Where(w => w.CITY1 == city).Select(s => s.ID).SingleOrDefault();

                                            CUSTOMER c = new CUSTOMER();
                                            c.CUSTOMERCODE = customer_code;
                                            c.FULLNAME = customer_name;
                                            c.GENDER = gender == "M" ? 1 : 0;
                                            c.PHONENUMBER = phone_number;
                                            c.PHONENUMBER2 = phone_number2;
                                            c.EMAIL = email;
                                            c.CITY = i_city;
                                            c.ADDRESS = address;
                                            c.PLACEOFPRACTICE1 = pop1;
                                            c.PLACEOFPRACTICE2 = pop2;
                                            c.PLACEOFPRACTICE3 = pop3;

                                            c.STATUS = (int)Status.Active;
                                            c.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());
                                            c.CREATEDDATE = DateTime.Now;
                                            db.CUSTOMERs.Add(c);
                                        }
                                        db.SaveChanges();

                                        dbTran.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        dbTran.Rollback();
                                        LogSystem.ErrorLog(ex, null);
                                        ViewBag.Error = "Data failed to insert";

                                        return View("Index", cUSTOMER.ToList());
                                    }

                                    ViewBag.Success = "Import Customer Success";
                                    return View("Index", cUSTOMER.ToList());
                                }
                            }
                            catch (Exception ex)
                            {
                                conn.Close();
                                throw;
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Error = "File type is incorrect";

                        var cUSTOMERs = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);
                        return View("Index", cUSTOMERs.ToList());
                    }

                }
            }
            catch (Exception ex)
            {
                //logging.ErrorLog(ex, null);
                LogSystem.ErrorLog(ex, null);
                ViewBag.Error = "Failed, your template file might be wrong. Please contact your Administrator!\n"+ex;

                var cUSTOMERs = db.CUSTOMERs.Include(p => p.USER).Where(w => w.CUSTOMERID != 0);
                return View("Index", cUSTOMERs.ToList());
            }

            //finally
            //{
            //    return View("Success");
            //}
        }

        // GET: Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            //ViewBag.CITY = Helpers.GetCityList();
            ViewBag.CITY = db.CITies.OrderBy(o => o.CITY1).ToList();

            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CUSTOMER cUSTOMER)
        {
            //ViewBag.CITY = Helpers.GetCityList();
            ViewBag.CITY = db.CITies.OrderBy(o => o.CITY1).ToList();

            CUSTOMER chkCustomer = db.CUSTOMERs.Where(w => w.CUSTOMERCODE == cUSTOMER.CUSTOMERCODE).FirstOrDefault();

            if (chkCustomer != null)
            {
                ModelState.AddModelError("CUSTOMERCODE", "Customer Code is already exists");
            }

            if (ModelState.IsValid)
            {
                cUSTOMER.CREATEDDATE = DateTime.Now;
                cUSTOMER.CREATEDBY = Convert.ToInt16(Session["UserID"].ToString());

                try
                {
                    db.CUSTOMERs.Add(cUSTOMER);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New Customer Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(cUSTOMER);
                }

                return RedirectToAction("Index");
            }

            return View(cUSTOMER);
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();

            return View(cUSTOMER);
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CUSTOMER cUSTOMER)
        {
            //ViewBag.Cities = Helpers.GetCityList();
            ViewBag.Cities = db.CITies.OrderBy(o => o.CITY1).ToList();


            CUSTOMER chkCustomer = db.CUSTOMERs.Where(w => w.CUSTOMERCODE == cUSTOMER.CUSTOMERCODE && w.CUSTOMERID != cUSTOMER.CUSTOMERID).FirstOrDefault();

            if (chkCustomer != null)
            {
                ModelState.AddModelError("CUSTOMERCODE", "Customer Code is already exists");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cUSTOMER).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update Customer Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(cUSTOMER);
                }

                return RedirectToAction(Request.Params["returnAction"] ?? "Index");
            }

            return View(cUSTOMER);
        }

        // GET: Customer/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
        //    if (cUSTOMER == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(cUSTOMER);
        //}

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            db.CUSTOMERs.Remove(cUSTOMER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // validate upload
        private bool Validate(DataTable dt)
        {
            int row = 1;
            int error = 0;
            string[,] msgArr = new string[dt.Rows.Count, 2];

            Dictionary<string, string> compDet = new Dictionary<string, string>();

            foreach (DataRow dr in dt.Rows)
            {
                int rowError = 0;
                string msg = "";

                // modify here
                string customer_code = dr["CUSTOMER_CODE"].ToString();
                string customer_name = dr["FULLNAME"].ToString();
                string gender = dr["GENDER"].ToString();
                string phone_number = dr["PHONE_NUMBER"].ToString();
                string phone_number2 = dr["PHONE_NUMBER2"].ToString();
                string email = dr["EMAIL"].ToString();
                string city = dr["CITY"].ToString();
                string address = dr["ADDRESS"].ToString();
                string pop1 = dr["PLACE_OF_PRACTICE1"].ToString();
                string pop2 = dr["PLACE_OF_PRACTICE2"].ToString();
                string pop3 = dr["PLACE_OF_PRACTICE3"].ToString();

                if (customer_code == "")
                {
                    msg += "[CUSTOMER_CODE] is required<br>";

                    rowError++;
                }
                else if (db.CUSTOMERs.Any(a => a.CUSTOMERCODE == customer_code))
                {
                    msg += "[CUSTOMER_CODE] <b>" + customer_code + "</b> already exist<br>";

                    rowError++;
                }

                if (customer_name == "")
                {
                    msg += "[FULLNAME] is required<br>";

                    rowError++;
                }

                if (gender == "")
                {
                    msg += "[GENDER] is required<br>";

                    rowError++;
                }
                else if (gender.ToUpper() != "M" && gender.ToUpper() != "F")
                {
                    msg += "[GENDER] must be M or F only<br>";

                    rowError++;
                }


                if (phone_number == "")
                {
                    msg += "[PHONE_NUMBER] is required<br>";

                    rowError++;
                }
                else if (!Regex.IsMatch(phone_number, "^[0-9]*$"))
                {
                    msg += "[PHONE_NUMBER] accepts only number<br>";

                    rowError++;
                }

                if (phone_number2 == "")
                {
                    msg += "[PHONE_NUMBER2] is required<br>";

                    rowError++;
                }
                else if (!Regex.IsMatch(phone_number2, "^[0-9]*$"))
                {
                    msg += "[PHONE_NUMBER2] accepts only number<br>";

                    rowError++;
                }

                if (email == "")
                {
                    msg += "[EMAIL] is required<br>";

                    rowError++;
                }
                else if (!Regex.IsMatch(email, "^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$"))
                {
                    msg += "[EMAIL] must be in correct format (ex: test@mail.com)<br>";

                    rowError++;
                }

                if (city == "")
                {
                    msg += "[CITY] is required<br>";

                    rowError++;
                }
                // TODO: change Get City List
                else if (!db.CITies.Any(a => a.CITY1 == city))
                {
                    msg += "[CITY] <b>" + city + "</b> is not found in database<br>";

                    rowError++;
                }

                if (address == "")
                {
                    msg += "[ADDRESS] is required<br>";

                    rowError++;
                }

                if (pop1 == "")
                {
                    msg += "[PLACE_OF_PRACTICE1] is required<br>";

                    rowError++;
                }
                // modify until here

                if (rowError > 0)
                {
                    msgArr[error, 0] = row.ToString();
                    msgArr[error, 1] = msg.EndsWith("<br>") ? msg.Substring(0, msg.Length - 4) : msg;
                    error++;
                }

                row++;
            }

            if (error > 0)
            {
                ViewBag.ErrorUpload = msgArr;
                return false;
            }
            else
                return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void Export()
        {
            List<CustomerExport> v = (from cus in db.CUSTOMERs
                                      join cit in db.CITies on cus.CITY equals cit.ID
                                     
                                      select new { cus, cit }).Select(s => new CustomerExport
                                      {
                                          CUSTOMER_CODE = s.cus.CUSTOMERCODE.ToString(),
                                          CUSTOMER_NAME = s.cus.FULLNAME.ToString(),
                                          GENDER = s.cus.GENDER.ToString() == "1" ? "MALE" : "FEMALE",
                                          PHONENUMBER = s.cus.PHONENUMBER.ToString(),
                                          PHONENUMBER2 = s.cus.PHONENUMBER2.ToString(),
                                          EMAIL = s.cus.EMAIL.ToString(),
                                          CITY = s.cit.CITY1.ToString(),
                                          ADDRESS = s.cus.ADDRESS.ToString(),
                                          PLACEOFPRACTICE1 = s.cus.PLACEOFPRACTICE1.ToString(),
                                          PLACEOFPRACTICE2 = s.cus.PLACEOFPRACTICE2.ToString(),
                                          PLACEOFPRACTICE3 = s.cus.PLACEOFPRACTICE3.ToString(),
                                          STATUS = s.cus.STATUS.ToString() == "1" ? "ACTIVE" : "INACTIVE",
                                      }).ToList();

            string filename = "[Contact Center] Customer Report_" + DateTime.Now.ToString("yyyyMMdd");

            var gv = new GridView();
            gv.DataSource = v;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }
    }
}
