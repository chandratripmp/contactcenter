using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;

namespace ContractCenter.Common
{
    public class Mailer
    {
        public static void SmtpSend(MailMessage mail)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(mail);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task SmtpSendAsync(MailMessage mail)
        {
            //Debug.WriteLine(DateTime.Now.ToString() + " Sending Mail [to: "+mail.To+"]...");
            try
            {
                using (var client = new SmtpClient())
                {
                    await client.SendMailAsync(mail);
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(DateTime.Now.ToString() + " Send Mail [to: " + mail.To + "] Failed!");
                ContactCenter.Common.LogSystem.ErrorLog(ex, null);
                //throw;
            }
            //Debug.WriteLine(DateTime.Now.ToString() + " Mail [to: " + mail.To + "] Sent!");
        }
        
        public static MailMessage ConstructMailMessage(string to, List<string> ccList, List<string> bccList, string subject, string msg, bool isHtml, string displayName = "", string replyTo = "", string attachment = "", string atttachmentName = "")
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            var mailSettings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

            //string from = mailSettings.Smtp.Network.UserName;
            string from = mailSettings.Smtp.From;

            MailAddress fromMailAddress = null;
            if (String.IsNullOrEmpty(displayName))
            {
                fromMailAddress = new MailAddress(from);
            }
            else
            {
                fromMailAddress = new MailAddress(from, displayName);
            }

            MailAddress toMailAddress = new MailAddress(to);

            MailMessage mail = new MailMessage(fromMailAddress, toMailAddress);
            mail.Subject = subject;
            mail.Body = msg;
            mail.IsBodyHtml = isHtml;

            if (!String.IsNullOrEmpty(replyTo))
            {
                mail.ReplyToList.Add(new MailAddress(replyTo));
            }

            if (!String.IsNullOrEmpty(attachment))
            {
                var attachmentFile = new Attachment(attachment);
                attachmentFile.Name = atttachmentName;
                mail.Attachments.Add(attachmentFile);
            }

            if (ccList != null)
            {
                foreach (string cc in ccList)
                {
                    if (!String.IsNullOrEmpty(cc))
                    {
                        mail.CC.Add(cc);
                    }

                }
            }

            if (bccList != null)
            {
                foreach (string bcc in bccList)
                {
                    if (!String.IsNullOrEmpty(bcc))
                    {
                        mail.Bcc.Add(bcc);
                    }
                }
            }

            return mail;
        }

        public static MailMessage ConstructMailMessage(List<string> toList, List<string> ccList, List<string> bccList, string subject, string msg, bool isHtml, string displayName = "", string replyTo = "", string attachment = "", string atttachmentName = "")
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            var mailSettings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

            string from = mailSettings.Smtp.Network.UserName;

            MailAddress fromMailAddress = null;
            if (String.IsNullOrEmpty(displayName))
            {
                fromMailAddress = new MailAddress(from);
            }
            else
            {
                fromMailAddress = new MailAddress(from, displayName);
            }

            //MailAddress toMailAddress = new MailAddress(to);

            //MailMessage mail = new MailMessage(fromMailAddress, toMailAddress);
            MailMessage mail = new MailMessage();
            mail.Subject = subject;
            mail.Body = msg;
            mail.IsBodyHtml = isHtml;
            mail.From = fromMailAddress;

            if (!String.IsNullOrEmpty(replyTo))
            {
                mail.ReplyToList.Add(new MailAddress(replyTo));
            }

            if (!String.IsNullOrEmpty(attachment))
            {
                var attachmentFile = new Attachment(attachment);
                attachmentFile.Name = atttachmentName;
                mail.Attachments.Add(attachmentFile);
            }

            if (toList != null)
            {
                foreach (string to in toList)
                {
                    if (!String.IsNullOrEmpty(to))
                    {
                        mail.To.Add(to);
                    }

                }
            }

            if (ccList != null)
            {
                foreach (string cc in ccList)
                {
                    if (!String.IsNullOrEmpty(cc))
                    {
                        mail.CC.Add(cc);
                    }

                }
            }

            if (bccList != null)
            {
                foreach (string bcc in bccList)
                {
                    if (!String.IsNullOrEmpty(bcc))
                    {
                        mail.Bcc.Add(bcc);
                    }
                }
            }

            return mail;
        }
    }
}