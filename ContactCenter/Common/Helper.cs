﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ContactCenter.Common
{
    #region Enum Collections
    
    public class ChannelType
    {
        private ChannelType(string value) { Value = value; }

        public string Value { get; set; }

        public static ChannelType Clinic { get { return new ChannelType("Clinic"); } }
        public static ChannelType Hospital { get { return new ChannelType("Hospital"); } }
        public static ChannelType Pharmacy { get { return new ChannelType("Pharmacy"); } }
    }

    public enum RecipientType
    {
        Customer = 1,
        Pharmacy = 2, 
        Distributor = 3
    }

    public enum OrderStatus
    {
        [Description("Submitted")]
        Submitted = 0,
        [Description("Completed")]
        Complete = 1,
        [Description("Draft")]
        Draft = 2,
        [Description("Inquiry")]
        Inquiry = 3,
        [Description("Confirmed")]
        Confirmed = 4,
        [Description("Rejected")]
        Rejected = 5,
    }
    
    public enum Status
    {
        [Display(Order = 1)]
        [Description("Active")]
        Active = 1,
        [Display(Order = 2)]
        [Description("Inactive")]
        Inactive = 0,
        [Display(Order = 3)]
        [Description("Approval")]
        Approval = 2,
        [Display(Order = 4)]
        [Description("Reject")]
        Reject = 3,
    }

    public enum UserRole
    {
        [Display(Order = 1)]
        [Description("Contact Center")]
        ContactCenter = 1,
        [Display(Order = 2)]
        [Description("Administrator")]
        Admin = 0
    }
    #endregion
    
    public static class Helpers
    {
        public static string AppName = "Halo Vaksin";

        public static void RemoveFor<TModel>(this ModelStateDictionary modelState, Expression<Func<TModel, object>> expression)
        {
            string expressionText = ExpressionHelper.GetExpressionText(expression);

            foreach (var ms in modelState.ToArray())
            {
                //if (ms.Key.StartsWith(expressionText + "."))
                if(ms.Key.Contains(expressionText + "."))
                {
                    modelState.Remove(ms);
                }
            }
        }

        public static Dictionary<string, string> GetCityList()
        {
            return new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"Jakarta Barat", "Jakarta Barat"},
                {"Jakarta Utara", "Jakarta Utara"},
                {"Jakarta Pusat", "Jakarta Pusat"},
                {"Jakarta Selatan", "Jakarta Selatan"},
                {"Jakarta Timur", "Jakarta Timur"},
            };
        }

        public static Dictionary<string, string> GetReasonList()
        {
            return new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"Out of Stock", "Out of Stock"},
                {"Outstanding Payment", "Outstanding Payment"},
                {"Credit Limit", "Credit Limit"},
                {"", "Others"}
            };
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }

    public static class Crypto
    {
        private static string _key = "$2y$12$DQB.DBeSkEppE8xoVgfu0.4QraH22kI71DyZLUmodeWCFIKhR9Qoq";
        public static string Encrypt(string msg)
        {
            // Convert hashed password to array
            byte[] key = Encoding.ASCII.GetBytes(_key);

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return EncryptString(msg, key, iv);
            
        }

        public static string Decrypt(string enc)
        {
            // Convert hashed password to array
            byte[] key = Encoding.ASCII.GetBytes(_key);

            // Create secret IV
            byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

            return DecryptString(enc, key, iv);
        }

        private static string EncryptString(string plainText, byte[] key, byte[] iv)
        {
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;

            // Set key and IV
            byte[] aesKey = new byte[32];
            Array.Copy(key, 0, aesKey, 0, 32);
            encryptor.Key = aesKey;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesEncryptor = encryptor.CreateEncryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write);

            // Convert the plainText string into a byte array
            byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

            // Encrypt the input plaintext string
            cryptoStream.Write(plainBytes, 0, plainBytes.Length);

            // Complete the encryption process
            cryptoStream.FlushFinalBlock();

            // Convert the encrypted data from a MemoryStream to a byte array
            byte[] cipherBytes = memoryStream.ToArray();

            // Close both the MemoryStream and the CryptoStream
            memoryStream.Close();
            cryptoStream.Close();

            // Convert the encrypted byte array to a base64 encoded string
            string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);

            // Return the encrypted data as a string
            return cipherText;
        }

        private static string DecryptString(string cipherText, byte[] key, byte[] iv)
        {
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;

            // Set key and IV
            byte[] aesKey = new byte[32];
            Array.Copy(key, 0, aesKey, 0, 32);
            encryptor.Key = aesKey;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesDecryptor = encryptor.CreateDecryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write);

            // Will contain decrypted plaintext
            string plainText = String.Empty;

            try
            {
                // Convert the ciphertext string into a byte array
                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                // Decrypt the input ciphertext string
                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

                // Complete the decryption process
                cryptoStream.FlushFinalBlock();

                // Convert the decrypted data from a MemoryStream to a byte array
                byte[] plainBytes = memoryStream.ToArray();

                // Convert the decrypted byte array to string
                plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
            }
            finally
            {
                // Close both the MemoryStream and the CryptoStream
                memoryStream.Close();
                cryptoStream.Close();
            }

            // Return the decrypted data as a string
            return plainText;
        }
    }
    
    public static class UserHelper
    {
        public static string DefaultPassword = "P@ssw0rd";
    }

    public static class EnumHelper
    {
        public static string GetDescription<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }
        
        public static List<SelectListItem> ToOrderedSelectList<TEnum>() where TEnum : struct, IConvertible
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>()
                .Select(@enum => new
                {
                    enumValue = GetValue(@enum),
                    order = GetDisplayOrder(@enum),
                    enumText = GetDisplayName(@enum)
                })
                .OrderBy(x => x.order)
                .Select(x => new SelectListItem
                {
                    Text = x.enumText,
                    Value = x.enumValue
                }
            ).ToList();
        }
        public static string GetValue<TEnum>(TEnum @enum) where TEnum : struct, IConvertible
        {
            return ((int)Enum.Parse(typeof(TEnum), @enum.ToString())).ToString();
        }

        private static string GetDisplayName<TEnum>(TEnum @enum) where TEnum : struct, IConvertible
        {
            return @enum.GetType().GetMember(@enum.ToString()).FirstOrDefault()?
                        .GetCustomAttribute<DisplayAttribute>(false)?.Name ?? @enum.ToString();
        }

        private static int GetDisplayOrder<TEnum>(TEnum @enum) where TEnum : struct, IConvertible
        {
            return @enum.GetType().GetMember(@enum.ToString()).FirstOrDefault()?
                        .GetCustomAttribute<DisplayAttribute>(false)?.Order ?? 0;
        }
    }
}