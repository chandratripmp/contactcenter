﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactCenter.Common
{
    public class ControlAuth
    {
        static string[] exceptions =
            {
                "User/Login"
                ,"User/Index"
                ,"User/Logout"
                ,"Error/Index"
                ,"Error/NotFound"
                ,"Error/NotPermitted"
            };
        
        public static void CheckControl(string areaName, string controllerName, string actionName, ActionExecutingContext filterContext)
        {
            string pageName = areaName != "" ? areaName + controllerName + "/" + actionName : controllerName + "/" + actionName;

            if (!exceptions.Contains(pageName))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var role = (UserRole) session["UserRole"];

                string[] adminMenu =
                {
                    "Product", "Pharmacy", "UserManagement", "Customer"
                };

                if (adminMenu.Contains(controllerName))
                {
                    if(role != UserRole.Admin)
                    {
                        filterContext.HttpContext.Response.Redirect("~/Error/NotPermitted", true);
                    }
                }
            }
        }

        public static void CheckSession(string pageName, ActionExecutingContext filterContext)
        {
            if (!exceptions.Contains(pageName) || pageName == "Home/Index")
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var user = session["UserId"];

                if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession))
                {
                    //send them off to the login page
                    var url = new UrlHelper(filterContext.RequestContext);
                    
                    var loginUrl = url.Content("~/User/Logout");

                    //if (pageName.Contains("Manage"))
                    //{
                    //    loginUrl = url.Content("~/Manage/Logout");
                    //}
                    
                    filterContext.HttpContext.Response.Redirect(loginUrl, true);
                }
            }
        }
    }
}