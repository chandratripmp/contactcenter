﻿using ContactCenter.Common;
using Cryptography;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ContractCenter.Common
{
    public class EmailRecipient
    {
        public string Name { get; set; }

        public ContactCenter.ORDER ORDER { get; set; }
        public List<ContactCenter.ORDERDETAIL> ORDERDETAILList { get; set; }

        public RecipientType RecipientType { get; set; }
    }

    public class MailTemplate
    {
        public static string ConstructOrderNotification(EmailRecipient recipient)
        {
            string filePath = HttpContext.Current.Server.MapPath("~/Content/OrderNotification.html");

            string s_replyInfo = ConfigurationManager.AppSettings["ReplyInfo"].ToString();
            string replyInfo = recipient.RecipientType == RecipientType.Distributor ? "<p><br />" + s_replyInfo + "</p>" : "";

            StringBuilder message = new StringBuilder(File.ReadAllText(filePath));

            string text = recipient.ORDER.STATUS == (int)OrderStatus.Rejected ? "Please be informed of below Purchase Order is being <b><u>REJECTED</u></b> via " + Helpers.AppName + ":" : (recipient.ORDER.STATUS == (int)OrderStatus.Complete ? "Please be informed of below Purchase Order is now <b><u>COMPLETED</u></b> via " + Helpers.AppName + ":" : "Please be informed of below Purchase Order via " + Helpers.AppName + ":");

            message = message.Replace("{{text}}"               , text);
            message = message.Replace("{{name}}"               , recipient.Name);
            message = message.Replace("{{order_id}}"           , recipient.ORDER.ORDERID.ToString());
            message = message.Replace("{{distributor_name}}"   , recipient.ORDER.DISTRIBUTORNAME);
            //message = message.Replace("{{distributor_address}}", recipient.ORDER.dis);
            message = message.Replace("{{distributor_phone}}"  , recipient.ORDER.DISTRIBUTORPHONENUMBER);
            message = message.Replace("{{customer_code}}"      , recipient.ORDER.CUSTOMER.CUSTOMERCODE);
            message = message.Replace("{{customer_name}}"      , recipient.ORDER.CUSTOMER.FULLNAME);
            message = message.Replace("{{customer_address}}"   , recipient.ORDER.CUSTOMER.ADDRESS);
            message = message.Replace("{{customer_phone}}"     , recipient.ORDER.CUSTOMER.PHONENUMBER);
            message = message.Replace("{{pharmacy_code}}"      , recipient.ORDER.PHARMACY.PHARMACYCODE);
            message = message.Replace("{{pharmacy_name}}"      , recipient.ORDER.PHARMACY.PHARMACYNAME);
            message = message.Replace("{{pharmacy_address}}"   , recipient.ORDER.PHARMACY.ADDRESS);
            message = message.Replace("{{pharmacy_phone}}"     , recipient.ORDER.PHARMACY.PHONENUMBER);
            message = message.Replace("{{delivery_address}}"   , recipient.ORDER.DELIVERYADDRESS);
            message = message.Replace("{{delivery_date}}"      , recipient.ORDER.DELIVERYDATE.Value.ToString("dd/MM/yyyy"));

            string discountHead = "";

            if (recipient.RecipientType != RecipientType.Customer)
            {
                discountHead =
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Channel %</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Price after Channel %</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Doctor %</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Price after Doctor %</th>";
            }
            else
            {
                discountHead =
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Doctor %</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Price after Doctor %</th>";
            }

            string tableProdHeader = 
                 "<tr>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Product Code</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Product Name</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Batch No</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Expired Date</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Plant</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Price</th>" +
                 discountHead +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Quantity</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>UoM</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Info Discount Customer</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Submission of Discount</th>" +
                 "    <th style='border: solid 1px black; padding: 2px 5px; '>Discount Reference</th>" +
                 "</tr>";

            string tableProdDetail = "", totalS = "";

            double total = 0, totalAD = 0;

            foreach(ContactCenter.ORDERDETAIL od in recipient.ORDERDETAILList)
            {
                string discount = "";
                total += Convert.ToDouble(od.PRICE * od.QTY);

                double discountD = Convert.ToDouble(((double)(od.DISCOUNT) / 100) * od.PRICE);
                double priceAD = Convert.ToDouble(od.PRICE - discountD);

                double discountD_cust = Convert.ToDouble(((double)(od.DISCOUNT_CUST) / 100) * od.PRICE);
                double priceAD_cust = Convert.ToDouble(od.PRICE - discountD_cust);

                if (recipient.RecipientType != RecipientType.Customer)
                {
                    totalAD += Convert.ToDouble(priceAD * od.QTY);
                    
                    discount =
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.DISCOUNT + "%</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>Rp" + String.Format("{0:#,#}", priceAD) + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.DISCOUNT_CUST + "%</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>Rp" + String.Format("{0:#,#}", priceAD_cust) + "</td>";
                }
                else
                {
                    discount =
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.DISCOUNT_CUST + "%</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>Rp" + String.Format("{0:#,#}", priceAD_cust) + "</td>";
                }
                
                tableProdDetail +=
                    "<tr>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.PRODUCT.PRODUCTCODE + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.PRODUCT.PRODUCTNAME + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.PRODUCT.BATCHNO + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.PRODUCT.EXPIREDDATE.Value.ToString("dd MMM yyyy") + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.PRODUCT.PLANT + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>Rp" + String.Format("{0:#,#}", od.PRICE) + "</td>" +
                    discount +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.QTY + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.UOM + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.INFODISCCUSTOMER + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.SUBMISSIONOFDISCOUNTS + "</td>" +
                    "    <td style='border: solid 1px black; padding: 2px 5px;'>" + od.DISCOUNTREFERENCE + "</td>" +
                    "</tr>";
            }

            totalS +=
                "<br><table>";
            //+ "   <tr><td><b>Total </b></td><td> : </td><td>Rp" + String.Format("{0:#,#}", total) + "</td></tr>";

            totalS += recipient.RecipientType != RecipientType.Customer ? "<tr><td><b>Total </b></td><td> : </td><td>Rp" + String.Format("{0:#,#}", total) + "</td></tr>" : "";

            totalS += recipient.RecipientType != RecipientType.Customer ? "<tr><td><b>Total after Discount</b></td><td> : </td><td>Rp" + String.Format("{0:#,#}", totalAD) + "</td></tr></table>" : "</table>";

            message = message.Replace("{{table_product}}"      , tableProdHeader + tableProdDetail);

            message = message.Replace("{{total_order}}", totalS);            
            
            message = message.Replace("{{reply_info}}"     , replyInfo);

            return message.ToString();
        }
    }
}