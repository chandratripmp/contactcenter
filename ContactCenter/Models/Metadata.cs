﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContactCenter
{
    public class PRODUCTMetadata
    {
        public int PRODUCTID { get; set; }

        [Required(ErrorMessage = "Product Code is required")]
        [Display(Name = "Product Code")]
        public string PRODUCTCODE { get; set; }

        [Required(ErrorMessage = "Product Name is required")]
        [Display(Name = "Product Name")]
        public string PRODUCTNAME { get; set; }
        
        [Required(ErrorMessage = "Plant is required")]
        [Display(Name = "Plant")]
        public string PLANT { get; set; }

        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Must be numeric")]
        [Required(ErrorMessage = "Price is required")]
        [Display(Name = "Price")]
        public double PRICE { get; set; }

        [RegularExpression("^([0-9]*)([,.][0-9]*)?$", ErrorMessage = "Stock must be a numeric")]
        [Required(ErrorMessage = "Stock is required")]
        [Display(Name = "Stock")]
        public int STOCK { get; set; }
        
        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }

        [Required(ErrorMessage = "Batch No is required")]
        [Display(Name = "Batch No")]
        public string BATCHNO { get; set; }
        
        [Required(ErrorMessage = "Expired Date is required")]
        [Display(Name = "Expired Date")]
        public Nullable<System.DateTime> EXPIREDDATE { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
    }

    public class USERMetadata
    {
        public int USERID { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Username")]
        public string USERNAME { get; set; }

        [Required(ErrorMessage = "Fullname is required")]
        [Display(Name = "Fullname")]
        public string FULLNAME { get; set; }

        [Required(ErrorMessage = "Role is required")]
        [Display(Name = "Role")]
        public int ROLE { get; set; }
        public string PASSWORD { get; set; }
        
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string EMAIL { get; set; }

        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }
    }

    public class CUSTOMERMetadata
    {
        public int CUSTOMERID { get; set; }

        [Required(ErrorMessage = "Customer Code is required")]
        [Display(Name = "Customer Code")]
        public string CUSTOMERCODE { get; set; }

        [Required(ErrorMessage = "Fullname is required")]
        [Display(Name = "Fullname")]
        public string FULLNAME { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        [Display(Name = "Gender")]
        public Nullable<int> GENDER { get; set; }
        
        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Phone Number")]
        public string PHONENUMBER { get; set; }

       
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Phone Number2")]
        public string PHONENUMBER2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CITY { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        public string ADDRESS { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string EMAIL { get; set; }

        [Required(ErrorMessage = "Place Of Practice 1 is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Place Of Practice 1")]
        public string PLACEOFPRACTICE1 { get; set; }
        
        [Display(Name = "Place Of Practice 2")]
        [DataType(DataType.MultilineText)]
        public string PLACEOFPRACTICE2 { get; set; }
        
        [Display(Name = "Place Of Practice 3")]
        [DataType(DataType.MultilineText)]
        public string PLACEOFPRACTICE3 { get; set; }

        [Required(ErrorMessage = "Status is required")]
        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }
        
        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }
    }

    public class PHARMACYMetadata
    {
        public int PHARMACYID { get; set; }

        [Required(ErrorMessage = "Channel Code is required")]
        [Display(Name = "Channel Code")]
        public string PHARMACYCODE { get; set; }

        [Required(ErrorMessage = "Channel Name is required")]
        [Display(Name = "Channel Name")]
        public string PHARMACYNAME { get; set; }

        [Required(ErrorMessage = "Channel Type is required")]
        [Display(Name = "Channel Type")]
        public string TYPE { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Phone Number")]
        public string PHONENUMBER { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number 2 must be a numeric")]
        [Display(Name = "Phone Number2")]
        public string PHONENUMBER2 { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string EMAIL { get; set; }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CITY { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        public string ADDRESS { get; set; }

        //[Required(ErrorMessage = "Plant is required")]
        [Display(Name = "Plant")]
        public string PHARMACYPLANT { get; set; }

        [Required(ErrorMessage = "Status is required")]
        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }

      
    }

    public class ORDERMetadata
    {
        [Display(Name = "Order ID")]
        public int ORDERID { get; set; }

        [Required(ErrorMessage = "Customer is required")]
        public int CUSTOMERID { get; set; }

        [Required(ErrorMessage = "Channel is required")]
        public int PHARMACYID { get; set; }

       
        [Display(Name = "Delivery Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> DELIVERYDATE { get; set; }

        [Display(Name = "Delivery Address")]
        public string DELIVERYADDRESS { get; set; }

        [Display(Name = "Order City")]
        public string CITY { get; set; }

        [Display(Name = "Distributor Name")]
        public string DISTRIBUTORNAME { get; set; }
        
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Distributor Email")]
        public string DISTRIBUTOREMAIL { get; set; }

        //[RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Distributor Phone")]
        public string DISTRIBUTORPHONENUMBER { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }

        [Display(Name = "Status")]
        public int STATUS { get; set; }

        [Display(Name = "Reject Reason")]
        public int REJECTREASON { get; set; }

        [Required(ErrorMessage = "User Channel is required")]
        [Display(Name = "User Channel")]
        public string USERCHANNEL { get; set; }

    }

    public class ORDERDETAILMetadata
    {
        [Display(Name = "Quantity")]
        public int QTY { get; set; }

        [Display(Name = "Price")]
        public Nullable<double> PRICE { get; set; }

        [Display(Name = "Panel Discount")]
        public Nullable<decimal> DISCOUNT { get; set; }

        [Display(Name = "Doctor Discount")]
        public Nullable<decimal> DISCOUNT_CUST { get; set; }
    }

    //[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Must be numeric")]
    //[Range(0, 100, ErrorMessage = "Must 0 to 100")]
    //public Nullable<double> ACQUISITIONCOST;

    //[RegularExpression("^[0-9]*$", ErrorMessage = "Work Period must be a numeric")]
    //[Display(Name = "Work Period")]

    //[Required(ErrorMessage = "Email is required")]
    //[Display(Name = "Email")]
    //[RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format.")]
    //public string SYSEMPEMAIL { get; set; }

    //[Range(0, Int16.MaxValue, ErrorMessage = "Age required non negative value")]
    //[Display(Name = "Age")]
    //public Nullable<int> AGE { get; set; }

    //[Required(ErrorMessage = "Date Of Agreement is required")]
    //[Display(Name = "Date Of Agreement")]
    //[DisplayFormat(DataFormatString = "dd/MM/yyyy")]
    //public Nullable<System.DateTime> PRICINGNOTEDATE;

    //[Required(ErrorMessage = "Sum Assured is required")]
    //[Display(Name = "Sum Assured")]
    //[DisplayFormat(DataFormatString = "{0:N0}")]
    //public double SUMASSURED { get; set; }

    public class DropdownLIST
    {
        public string Text { get; set; }
        public string Value { get; set; }


    }
}