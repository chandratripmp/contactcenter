﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ContactCenter
{
    [MetadataType(typeof(PRODUCTMetadata))]
    public partial class PRODUCT { }

    [MetadataType(typeof(USERMetadata))]
    public partial class USER { }

    [MetadataType(typeof(CUSTOMERMetadata))]
    public partial class CUSTOMER { }
    
    [MetadataType(typeof(PHARMACYMetadata))]
    public partial class PHARMACY { }
    
    [MetadataType(typeof(ORDERMetadata))]
    public partial class ORDER { }

    [MetadataType(typeof(ORDERDETAILMetadata))]
    public partial class ORDERDETAIL { }


}