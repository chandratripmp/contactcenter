﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContactCenter.Models
{
    public class OrderViewModel
    {
        public ORDER ORDER { get; set; }
        public List<ORDERDETAIL> ORDERDETAILList { get; set; }
        public List<CUSTOMER> CustList { get; set; }
        public List<PHARMACY> PharList { get; set; }
        public List<PRODUCT> ProdList { get; set; }
        public List<CITY> Cities { get; set; }
        public int SubmitType { get; set; }
        public string InquiryType { get; set; }
        public string InquiryNotes { get; set; }
        public string ChannelType { get; set; }
        public string Title { get; set; }
    }

    public class OrderIndexViewModel
    {
        public ORDER ORDER { get; set; }
        public PHARMACY PHARMACY { get; set; }
        public CUSTOMER CUSTOMER { get; set; }
        public PRODUCT PRODUCT { get; set; }
        public ORDERDETAIL ORDERDETAIL { get; set; }

        public CALLBACK CALLBACK { get; set; }
        public callbackjoin callbackjoin { get; set; }
        public int S_ORDERID { get; set; }
        public string S_DIST_NAME { get; set; }
        public string S_DIST_PHONE { get; set; }
        public string S_DIST_EMAIL { get; set; }

        public string S_CUST_CODE { get; set; }
        public string S_CUST_NAME { get; set; }
        public string S_CUST_PHONE { get; set; }
        public string S_DELIV_ADDRES { get; set; }

        public string S_PHAR_CODE { get; set; }
        public string S_PHAR_NAME { get; set; }
        public string S_PHAR_ADDRESS { get; set; }
        public string S_PHAR_PHONE { get; set; }
        public string S_PHAR_EMAIL { get; set; }
        public string S_CITY { get; set; }

        public string S_DELIV_DATE { get; set; }
        public string S_STATUS { get; set; }
        public string I_STATUS { get; set; }
        public int I_CREATEDBY { get; set; }
        public string S_CREATEDBY { get; set; }
        public string S_CREATEDDATE { get; set; }
        public string O_CREATEDDATE { get; set; }
        public DateTime D_CREATEDDATE { get; set; }

        public string S_PROD_CODE { get; set; }
        public string S_PROD_NAME { get; set; }
        public string S_BATCHNO { get; set; }
        public string S_EXPDATE { get; set; }
        public string S_PLANT { get; set; }
        public string S_PRICE { get; set; }
        public string S_DISCOUNT { get; set; }
        public string S_QTY { get; set; }
        public string S_UOM { get; set; }

        public string SOURCEORDER { get; set; }
        public string INFODISCCUSTOMER { get; set; }
        public string SUBMISSIONOFDISCOUNTS { get; set; }
        public string DISCOUNTREFERENCE { get; set; }
        public string USERCHANNEL { get; set; }

        public Nullable<int> CNT { get; set; }

    }

    public class OrderExport
    {
        public string ORDER_ID { get; set; }
        public string DISTRIBUTOR_NAME { get; set; }
        public string DISTRIBUTOR_PHONE { get; set; }
        public string DISTRIBUTOR_EMAIL { get; set; }

        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_PHONE { get; set; }
        public string DELIVERY_ADDRESS { get; set; }

        public string PHARMACY_CODE { get; set; }
        public string PHARMACY_NAME { get; set; }
        public string PHARMACY_ADDRESS { get; set; }
        public string PHARMACY_PHONE { get; set; }
        public string PHARMACY_EMAIL { get; set; }
        public string CITY { get; set; }

        public string DELIVERY_DATE { get; set; }
        public string STATUS { get; set; }
        public string REJECT_REASON { get; set; }
        public string INQUIRY_TYPE { get; set; }
        public string INQUIRY_NOTES { get; set; }
        public string CREATED_BY { get; set; }
        public string ORDER_DATE { get; set; }

        public string PRODUCT_CODE { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string BATCHNO { get; set; }
        public string EXPDATE { get; set; }
        public string PLANT { get; set; }
        public string PRICE { get; set; }
        public string DISCOUNT { get; set; }
        public string DISCOUNT_CUSTOMER { get; set; }
        public string QTY { get; set; }
        public string UOM { get; set; }

        public string SOURCEORDER { get; set; }
        public string INFODISCCUSTOMER { get; set; }
        public string SUBMISSIONOFDISCOUNTS { get; set; }
        public string DISCOUNTREFERENCE { get; set; }
        public string USERCHANNEL { get; set; }
        public string SO { get; set; }
        public string DO { get; set; }
        public string UPDATESODO { get; set; }
        public string GROSS_SALES { get; set; }
        public string NETT_SALES { get; set; }
    }

    public class NewOrderExport
    {
        public string TANGGAL { get; set; }
        public string NAMA_TELESELLER { get; set; }
        public string SUMBER_ORDER { get; set; }
        public string CABANG_APL { get; set; }
        public string KODE_CUSTOMER { get; set; }
        public string NAMA_CUSTOMER { get; set; }
        public string USER { get; set; }
        public string PRODUK { get; set; }
        public string QTY { get; set; }
        public string DISKON_CUSTOMER { get; set; }
        public string KET_DISC_CUS { get; set; }
        public string DISC_PANEL { get; set; }
        public string PENGAJUAN_DISC { get; set; }
        public string SO { get; set; }
        public string DO { get; set; }
        public string STATUS { get; set; }
        public string ISSUE { get; set; }
        public string CALLBACK_KE_CUS { get; set; }
        public string GROSS_SALES { get; set; }
        public string NETT_SALES { get; set; }

    }
    public class CustomerIndexViewModel
    {
        public CUSTOMER CUSTOMER { get; set; }
        public CITY CITY { get; set; }

        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string GENDER { get; set; }
        public string PHONENUMBER { get; set; }
        public string EMAIL { get; set; }
        public string CITY_ID { get; set; }
        public string ADDRESS { get; set; }
        public string PLACEOFPRACTICE1 { get; set; }
        public string PLACEOFPRACTICE2 { get; set; }
        public string PLACEOFPRACTICE3 { get; set; }
        public string STATUS { get; set; }

        public string USER { get; set; }


    }

    public class CustomerExport
    {
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string GENDER { get; set; }
        public string PHONENUMBER { get; set; }
        public string PHONENUMBER2 { get; set; }
        public string EMAIL { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string PLACEOFPRACTICE1 { get; set; }
        public string PLACEOFPRACTICE2 { get; set; }
        public string PLACEOFPRACTICE3 { get; set; }
        public string STATUS { get; set; }


    }

    public class ChannelExport
    {
        public string CHANNEL_CODE { get; set; }
        public string CHANNEL_NAME { get; set; }
        public string PHONENUMBER { get; set; }
        public string PHONENUMBER2 { get; set; }
        public string EMAIL { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string TYPE { get; set; }
        public string PLANT { get; set; }
    }

    public class CallbackExport
    {
        public int IDCB { get; set; }
        public string ORDERID { get; set; }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        public string CREATEDDATE { get; set; }

    }

    public class callbackjoin
    {
        public int? orderid { get; set; }
        public int CNT { get; set; }
    }
}